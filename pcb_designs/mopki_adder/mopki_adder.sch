EESchema Schematic File Version 4
LIBS:mopki_adder-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR02
U 1 1 5B1951F8
P 11100 6300
F 0 "#PWR02" H 11100 6050 50  0001 C CNN
F 1 "GND" H 11105 6127 50  0000 C CNN
F 2 "" H 11100 6300 50  0001 C CNN
F 3 "" H 11100 6300 50  0001 C CNN
	1    11100 6300
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR01
U 1 1 5B1952DF
P 10750 6300
F 0 "#PWR01" H 10750 6150 50  0001 C CNN
F 1 "VCC" H 10767 6473 50  0000 C CNN
F 2 "" H 10750 6300 50  0001 C CNN
F 3 "" H 10750 6300 50  0001 C CNN
	1    10750 6300
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5B19534E
P 11100 6300
F 0 "#FLG02" H 11100 6375 50  0001 C CNN
F 1 "PWR_FLAG" H 11100 6474 50  0000 C CNN
F 2 "" H 11100 6300 50  0001 C CNN
F 3 "~" H 11100 6300 50  0001 C CNN
	1    11100 6300
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5B195370
P 10750 6300
F 0 "#FLG01" H 10750 6375 50  0001 C CNN
F 1 "PWR_FLAG" H 10750 6473 50  0000 C CNN
F 2 "" H 10750 6300 50  0001 C CNN
F 3 "~" H 10750 6300 50  0001 C CNN
	1    10750 6300
	-1   0    0    1   
$EndComp
Text GLabel 1250 1700 2    50   Input ~ 0
GND
Text GLabel 1300 950  2    50   Input ~ 0
GND
Text GLabel 800  950  0    50   Input ~ 0
VCC
Text GLabel 750  1700 0    50   Input ~ 0
VCC
Text Label 2000 2300 0    50   ~ 0
Carry_In
Text GLabel 10750 6300 0    50   Input ~ 0
VCC
Text GLabel 11100 6300 0    50   Input ~ 0
GND
Text GLabel 2400 950  2    50   Input ~ 0
GND
Text GLabel 1900 950  0    50   Input ~ 0
VCC
$Comp
L 74xx:74LS283 U3
U 1 1 5B324646
P 4800 1500
F 0 "U3" H 4600 2150 50  0000 C CNN
F 1 "74LS283" H 5050 2150 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 4800 1500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS283" H 4800 1500 50  0001 C CNN
	1    4800 1500
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS283 U6
U 1 1 5B324770
P 4800 3600
F 0 "U6" H 4600 4250 50  0000 C CNN
F 1 "74LS283" H 5000 4250 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 4800 3600 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS283" H 4800 3600 50  0001 C CNN
	1    4800 3600
	1    0    0    -1  
$EndComp
Text Label 4300 1000 2    50   ~ 0
Carry_In
Wire Wire Line
	4300 3100 4300 2500
Wire Wire Line
	4300 2500 5300 2500
Wire Wire Line
	5300 2500 5300 1500
$Comp
L power:GND #PWR04
U 1 1 5B3249D5
P 4800 2300
F 0 "#PWR04" H 4800 2050 50  0001 C CNN
F 1 "GND" H 4805 2127 50  0000 C CNN
F 2 "" H 4800 2300 50  0001 C CNN
F 3 "" H 4800 2300 50  0001 C CNN
	1    4800 2300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5B324A28
P 4800 4400
F 0 "#PWR07" H 4800 4150 50  0001 C CNN
F 1 "GND" H 4805 4227 50  0000 C CNN
F 2 "" H 4800 4400 50  0001 C CNN
F 3 "" H 4800 4400 50  0001 C CNN
	1    4800 4400
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR05
U 1 1 5B324ABA
P 4800 2800
F 0 "#PWR05" H 4800 2650 50  0001 C CNN
F 1 "VCC" H 4817 2973 50  0000 C CNN
F 2 "" H 4800 2800 50  0001 C CNN
F 3 "" H 4800 2800 50  0001 C CNN
	1    4800 2800
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR03
U 1 1 5B324B22
P 4800 700
F 0 "#PWR03" H 4800 550 50  0001 C CNN
F 1 "VCC" H 4817 873 50  0000 C CNN
F 2 "" H 4800 700 50  0001 C CNN
F 3 "" H 4800 700 50  0001 C CNN
	1    4800 700 
	1    0    0    -1  
$EndComp
Text Label 800  1050 2    50   ~ 0
Arg_A0
Text Label 1300 1050 0    50   ~ 0
Arg_A1
Text Label 800  1150 2    50   ~ 0
Arg_A2
Text Label 1300 1150 0    50   ~ 0
Arg_A3
Text Label 800  1250 2    50   ~ 0
Arg_A4
Text Label 1300 1250 0    50   ~ 0
Arg_A5
Text Label 800  1350 2    50   ~ 0
Arg_A6
Text Label 1300 1350 0    50   ~ 0
Arg_A7
Text Label 1900 1050 2    50   ~ 0
Arg_B0
Text Label 2400 1050 0    50   ~ 0
Arg_B1
Text Label 1900 1150 2    50   ~ 0
Arg_B2
Text Label 2400 1150 0    50   ~ 0
Arg_B3
Text Label 1900 1250 2    50   ~ 0
Arg_B4
Text Label 2400 1250 0    50   ~ 0
Arg_B5
Text Label 1900 1350 2    50   ~ 0
Arg_B6
Text Label 2400 1350 0    50   ~ 0
Arg_B7
Text Label 750  1800 2    50   ~ 0
Sum0
Text Label 1250 1800 0    50   ~ 0
Sum1
Text Label 750  1900 2    50   ~ 0
Sum2
Text Label 1250 1900 0    50   ~ 0
Sum3
Text Label 750  2000 2    50   ~ 0
Sum4
Text Label 1250 2000 0    50   ~ 0
Sum5
Text Label 750  2100 2    50   ~ 0
Sum6
Text Label 1250 2100 0    50   ~ 0
Sum7
Text Label 2000 1850 0    50   ~ 0
Carry_Out
Text Label 5300 3600 0    50   ~ 0
Carry_Out
Text Label 5300 1000 0    50   ~ 0
Sum0
Text Label 5300 3100 0    50   ~ 0
Sum4
Text Label 5300 3200 0    50   ~ 0
Sum5
Text Label 5300 3300 0    50   ~ 0
Sum6
Text Label 5300 3400 0    50   ~ 0
Sum7
Text Label 4300 1200 2    50   ~ 0
Arg_A0
Text Label 4300 1300 2    50   ~ 0
Arg_A1
Text Label 4300 1400 2    50   ~ 0
Arg_A2
Text Label 4300 1500 2    50   ~ 0
Arg_A3
Text Label 4300 3300 2    50   ~ 0
Arg_A4
Text Label 4300 3400 2    50   ~ 0
Arg_A5
Text Label 4300 3500 2    50   ~ 0
Arg_A6
Text Label 4300 3600 2    50   ~ 0
Arg_A7
Text Label 4300 1700 2    50   ~ 0
Arg_B0
Text Label 4300 1800 2    50   ~ 0
Arg_B1
Text Label 4300 1900 2    50   ~ 0
Arg_B2
Text Label 4300 2000 2    50   ~ 0
Arg_B3
Text Label 4300 3800 2    50   ~ 0
Arg_B4
Text Label 4300 3900 2    50   ~ 0
Arg_B5
Text Label 4300 4000 2    50   ~ 0
Arg_B6
Text Label 4300 4100 2    50   ~ 0
Arg_B7
$Comp
L 74xx:74HC04 U1
U 1 1 5B32548C
P 6750 1000
F 0 "U1" H 6750 1317 50  0000 C CNN
F 1 "74HC04" H 6750 1226 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 6750 1000 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/74HC_HCT04.pdf" H 6750 1000 50  0001 C CNN
	1    6750 1000
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC04 U1
U 2 1 5B3254FF
P 6750 1300
F 0 "U1" H 6750 1617 50  0000 C CNN
F 1 "74HC04" H 6750 1526 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 6750 1300 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/74HC_HCT04.pdf" H 6750 1300 50  0001 C CNN
	2    6750 1300
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC04 U1
U 3 1 5B32554B
P 6750 1600
F 0 "U1" H 6750 1917 50  0000 C CNN
F 1 "74HC04" H 6750 1826 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 6750 1600 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/74HC_HCT04.pdf" H 6750 1600 50  0001 C CNN
	3    6750 1600
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC04 U1
U 4 1 5B32559C
P 6750 1900
F 0 "U1" H 6750 2217 50  0000 C CNN
F 1 "74HC04" H 6750 2126 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 6750 1900 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/74HC_HCT04.pdf" H 6750 1900 50  0001 C CNN
	4    6750 1900
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC04 U1
U 5 1 5B3255EE
P 3500 6300
F 0 "U1" H 3500 6617 50  0000 C CNN
F 1 "74HC04" H 3500 6526 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 3500 6300 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/74HC_HCT04.pdf" H 3500 6300 50  0001 C CNN
	5    3500 6300
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC04 U1
U 6 1 5B325643
P 7100 6100
F 0 "U1" H 7100 6417 50  0000 C CNN
F 1 "74HC04" H 7100 6326 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 7100 6100 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/74HC_HCT04.pdf" H 7100 6100 50  0001 C CNN
	6    7100 6100
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC04 U5
U 7 1 5B32569D
P 10600 4250
F 0 "U5" H 10830 4296 50  0000 L CNN
F 1 "74HC04" H 10830 4205 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 10600 4250 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/74HC_HCT04.pdf" H 10600 4250 50  0001 C CNN
	7    10600 4250
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC04 U5
U 1 1 5B326126
P 6750 3100
F 0 "U5" H 6750 3417 50  0000 C CNN
F 1 "74HC04" H 6750 3326 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 6750 3100 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/74HC_HCT04.pdf" H 6750 3100 50  0001 C CNN
	1    6750 3100
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC04 U5
U 2 1 5B32612D
P 6750 3400
F 0 "U5" H 6750 3717 50  0000 C CNN
F 1 "74HC04" H 6750 3626 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 6750 3400 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/74HC_HCT04.pdf" H 6750 3400 50  0001 C CNN
	2    6750 3400
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC04 U5
U 3 1 5B326134
P 6750 3700
F 0 "U5" H 6750 4017 50  0000 C CNN
F 1 "74HC04" H 6750 3926 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 6750 3700 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/74HC_HCT04.pdf" H 6750 3700 50  0001 C CNN
	3    6750 3700
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC04 U5
U 4 1 5B32613B
P 6750 4000
F 0 "U5" H 6750 4317 50  0000 C CNN
F 1 "74HC04" H 6750 4226 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 6750 4000 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/74HC_HCT04.pdf" H 6750 4000 50  0001 C CNN
	4    6750 4000
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC04 U5
U 5 1 5B326142
P 5050 5700
F 0 "U5" H 5050 6017 50  0000 C CNN
F 1 "74HC04" H 5050 5926 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 5050 5700 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/74HC_HCT04.pdf" H 5050 5700 50  0001 C CNN
	5    5050 5700
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC04 U5
U 6 1 5B326149
P 3500 6850
F 0 "U5" H 3500 7167 50  0000 C CNN
F 1 "74HC04" H 3500 7076 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 3500 6850 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/74HC_HCT04.pdf" H 3500 6850 50  0001 C CNN
	6    3500 6850
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC04 U1
U 7 1 5B3261AE
P 10150 4250
F 0 "U1" H 10380 4296 50  0000 L CNN
F 1 "74HC04" H 10380 4205 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 10150 4250 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/74HC_HCT04.pdf" H 10150 4250 50  0001 C CNN
	7    10150 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 3200 6450 3200
Wire Wire Line
	6450 3200 6450 3400
Wire Wire Line
	5300 3300 6400 3300
Wire Wire Line
	6400 3700 6450 3700
Wire Wire Line
	5300 3400 6350 3400
Wire Wire Line
	6350 3400 6350 4000
Wire Wire Line
	6350 4000 6450 4000
Wire Wire Line
	6450 1100 6450 1300
Wire Wire Line
	6400 1200 6400 1600
Wire Wire Line
	6400 1600 6450 1600
Wire Wire Line
	6300 1300 6300 1900
Wire Wire Line
	6300 1900 6450 1900
$Comp
L 74xx:74LS21 U2
U 1 1 5B3272B9
P 7400 1450
F 0 "U2" H 7400 1825 50  0000 C CNN
F 1 "74LS21" H 7400 1734 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 7400 1450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS21" H 7400 1450 50  0001 C CNN
	1    7400 1450
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS21 U2
U 2 1 5B32737C
P 7400 3550
F 0 "U2" H 7400 3925 50  0000 C CNN
F 1 "74LS21" H 7400 3834 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 7400 3550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS21" H 7400 3550 50  0001 C CNN
	2    7400 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 3100 6450 3100
Wire Wire Line
	6400 3300 6400 3700
Wire Wire Line
	7050 3400 7050 3500
Wire Wire Line
	7050 3500 7100 3500
Wire Wire Line
	7050 3700 7050 3600
Wire Wire Line
	7050 3600 7100 3600
Wire Wire Line
	7100 3100 7050 3100
Wire Wire Line
	7100 4000 7050 4000
Wire Wire Line
	7050 1300 7050 1400
Wire Wire Line
	7050 1400 7100 1400
Wire Wire Line
	7050 1600 7050 1500
Wire Wire Line
	7050 1500 7100 1500
Wire Wire Line
	7100 1300 7100 1000
Wire Wire Line
	7100 1000 7050 1000
Wire Wire Line
	7100 1600 7100 1900
Wire Wire Line
	7100 1900 7050 1900
Wire Wire Line
	7100 3700 7100 4000
Wire Wire Line
	7100 3100 7100 3400
Wire Wire Line
	10150 3750 10600 3750
Connection ~ 10150 3750
Wire Wire Line
	10150 4750 10600 4750
Connection ~ 10150 4750
$Comp
L power:VCC #PWR06
U 1 1 5B32E1D9
P 10150 3750
F 0 "#PWR06" H 10150 3600 50  0001 C CNN
F 1 "VCC" H 10167 3923 50  0000 C CNN
F 2 "" H 10150 3750 50  0001 C CNN
F 3 "" H 10150 3750 50  0001 C CNN
	1    10150 3750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR08
U 1 1 5B32E24A
P 10150 4750
F 0 "#PWR08" H 10150 4500 50  0001 C CNN
F 1 "GND" H 10155 4577 50  0000 C CNN
F 2 "" H 10150 4750 50  0001 C CNN
F 3 "" H 10150 4750 50  0001 C CNN
	1    10150 4750
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS08 U4
U 1 1 5B32E476
P 8000 2450
F 0 "U4" H 8000 2775 50  0000 C CNN
F 1 "74LS08" H 8000 2684 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 8000 2450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 8000 2450 50  0001 C CNN
	1    8000 2450
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS08 U4
U 2 1 5B32E615
P 9150 5850
F 0 "U4" H 9150 6175 50  0000 C CNN
F 1 "74LS08" H 9150 6084 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 9150 5850 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 9150 5850 50  0001 C CNN
	2    9150 5850
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS08 U4
U 3 1 5B32E6AE
P 7100 5600
F 0 "U4" H 7100 5925 50  0000 C CNN
F 1 "74LS08" H 7100 5834 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 7100 5600 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 7100 5600 50  0001 C CNN
	3    7100 5600
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS08 U4
U 4 1 5B32E774
P 6500 5500
F 0 "U4" H 6500 5825 50  0000 C CNN
F 1 "74LS08" H 6500 5734 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 6500 5500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 6500 5500 50  0001 C CNN
	4    6500 5500
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS08 U4
U 5 1 5B32E7FB
P 9250 4250
F 0 "U4" H 9480 4296 50  0000 L CNN
F 1 "74LS08" H 9480 4205 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 9250 4250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS08" H 9250 4250 50  0001 C CNN
	5    9250 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	7700 3550 7700 2550
Wire Wire Line
	7700 1450 7700 2350
Wire Wire Line
	5300 1000 6450 1000
Wire Wire Line
	5300 1100 6450 1100
Wire Wire Line
	5300 1200 6400 1200
Wire Wire Line
	5300 1300 6300 1300
Text Label 2000 1950 0    50   ~ 0
Zero
Text Label 8300 2450 0    50   ~ 0
Zero
Text Label 5300 1100 0    50   ~ 0
Sum1
Text Label 5300 1200 0    50   ~ 0
Sum2
Text Label 5300 1300 0    50   ~ 0
Sum3
Text Label 4750 5600 2    50   ~ 0
Arg_B7
Text Label 4750 5500 2    50   ~ 0
Arg_A7
$Comp
L 74xx:74LS21 U2
U 3 1 5B334234
P 8800 4250
F 0 "U2" H 9030 4296 50  0000 L CNN
F 1 "74LS21" H 9030 4205 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 8800 4250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS21" H 8800 4250 50  0001 C CNN
	3    8800 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 3750 9250 3750
Connection ~ 9250 3750
Wire Wire Line
	8800 4750 9250 4750
Connection ~ 9250 4750
Text Label 4750 5700 2    50   ~ 0
Carry_Out
$Comp
L power:VCC #PWR09
U 1 1 5B33A639
P 6200 5800
F 0 "#PWR09" H 6200 5650 50  0001 C CNN
F 1 "VCC" V 6218 5927 50  0000 L CNN
F 2 "" H 6200 5800 50  0001 C CNN
F 3 "" H 6200 5800 50  0001 C CNN
	1    6200 5800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6200 5500 5400 5500
Wire Wire Line
	6200 5600 5450 5600
Wire Wire Line
	5600 6100 5450 6100
Wire Wire Line
	5450 6100 5450 5600
Connection ~ 5450 5600
Wire Wire Line
	5450 5600 4750 5600
Wire Wire Line
	5600 6200 5400 6200
Wire Wire Line
	5400 6200 5400 5500
Connection ~ 5400 5500
Wire Wire Line
	5400 5500 4750 5500
Text Label 8000 5850 0    50   ~ 0
Overflow
Text Label 2000 2050 0    50   ~ 0
Overflow
Wire Wire Line
	2000 1750 2950 1750
Text Label 2950 1750 0    50   ~ 0
Sum7
Text Label 2000 1750 0    50   ~ 0
Negative
NoConn ~ 8850 5750
NoConn ~ 8850 5950
NoConn ~ 9450 5850
$Comp
L 74xx:74LS32 U8
U 1 1 5B35EBE0
P 5900 6200
F 0 "U8" H 5900 6525 50  0000 C CNN
F 1 "74LS32" H 5900 6434 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 5900 6200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 5900 6200 50  0001 C CNN
	1    5900 6200
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U8
U 2 1 5B35ECA0
P 6500 6100
F 0 "U8" H 6500 6425 50  0000 C CNN
F 1 "74LS32" H 6500 6334 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 6500 6100 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 6500 6100 50  0001 C CNN
	2    6500 6100
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U8
U 3 1 5B35EDE9
P 7700 5850
F 0 "U8" H 7700 6175 50  0000 C CNN
F 1 "74LS32" H 7700 6084 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 7700 5850 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 7700 5850 50  0001 C CNN
	3    7700 5850
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U8
U 4 1 5B35EE7D
P 9150 6200
F 0 "U8" H 9150 6525 50  0000 C CNN
F 1 "74LS32" H 9150 6434 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 9150 6200 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 9150 6200 50  0001 C CNN
	4    9150 6200
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS32 U8
U 5 1 5B35EF16
P 8350 4250
F 0 "U8" H 8580 4296 50  0000 L CNN
F 1 "74LS32" H 8580 4205 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 8350 4250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS32" H 8350 4250 50  0001 C CNN
	5    8350 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	6200 5500 6200 5400
Wire Wire Line
	5600 6200 5600 6300
Wire Wire Line
	5350 5700 5600 5700
Wire Wire Line
	5600 5700 5600 6000
Wire Wire Line
	5600 6000 6200 6000
Connection ~ 5600 5700
Wire Wire Line
	5600 5700 6800 5700
Wire Wire Line
	7400 6100 7400 5950
Wire Wire Line
	7400 5750 7400 5600
NoConn ~ 8850 6100
NoConn ~ 8850 6300
NoConn ~ 9450 6200
Wire Wire Line
	8800 3750 8350 3750
Connection ~ 8800 3750
Wire Wire Line
	8800 4750 8350 4750
Connection ~ 8800 4750
NoConn ~ 3200 6850
NoConn ~ 3200 6300
NoConn ~ 3800 6300
NoConn ~ 3800 6850
Wire Wire Line
	9250 3750 10150 3750
Wire Wire Line
	9250 4750 10150 4750
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J1
U 1 1 5B3A5DD9
P 1000 1150
F 0 "J1" H 1050 1567 50  0000 C CNN
F 1 "Argument_A" H 1050 1476 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 1000 1150 50  0001 C CNN
F 3 "~" H 1000 1150 50  0001 C CNN
	1    1000 1150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J2
U 1 1 5B3A77DA
P 2100 1150
F 0 "J2" H 2150 1567 50  0000 C CNN
F 1 "Argument_B" H 2150 1476 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 2100 1150 50  0001 C CNN
F 3 "~" H 2100 1150 50  0001 C CNN
	1    2100 1150
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J3
U 1 1 5B3A925F
P 950 1900
F 0 "J3" H 1000 2317 50  0000 C CNN
F 1 "Result" H 1000 2226 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 950 1900 50  0001 C CNN
F 3 "~" H 950 1900 50  0001 C CNN
	1    950  1900
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Male J4
U 1 1 5B3AAC97
P 1800 1850
F 0 "J4" H 1906 2128 50  0000 C CNN
F 1 "Flags" H 1906 2037 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 1800 1850 50  0001 C CNN
F 3 "~" H 1800 1850 50  0001 C CNN
	1    1800 1850
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x01_Male J5
U 1 1 5B3AE5F4
P 1800 2300
F 0 "J5" H 1906 2478 50  0000 C CNN
F 1 "Carry_In" H 1906 2387 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 1800 2300 50  0001 C CNN
F 3 "~" H 1800 2300 50  0001 C CNN
	1    1800 2300
	1    0    0    -1  
$EndComp
$EndSCHEMATC
