EESchema Schematic File Version 4
LIBS:mopki_bus-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR02
U 1 1 5B1951F8
P 11100 6300
F 0 "#PWR02" H 11100 6050 50  0001 C CNN
F 1 "GND" H 11105 6127 50  0000 C CNN
F 2 "" H 11100 6300 50  0001 C CNN
F 3 "" H 11100 6300 50  0001 C CNN
	1    11100 6300
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR01
U 1 1 5B1952DF
P 10750 6300
F 0 "#PWR01" H 10750 6150 50  0001 C CNN
F 1 "VCC" H 10767 6473 50  0000 C CNN
F 2 "" H 10750 6300 50  0001 C CNN
F 3 "" H 10750 6300 50  0001 C CNN
	1    10750 6300
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5B19534E
P 11100 6300
F 0 "#FLG02" H 11100 6375 50  0001 C CNN
F 1 "PWR_FLAG" H 11100 6474 50  0000 C CNN
F 2 "" H 11100 6300 50  0001 C CNN
F 3 "~" H 11100 6300 50  0001 C CNN
	1    11100 6300
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5B195370
P 10750 6300
F 0 "#FLG01" H 10750 6375 50  0001 C CNN
F 1 "PWR_FLAG" H 10750 6473 50  0000 C CNN
F 2 "" H 10750 6300 50  0001 C CNN
F 3 "~" H 10750 6300 50  0001 C CNN
	1    10750 6300
	-1   0    0    1   
$EndComp
Text GLabel 2200 2450 2    50   Input ~ 0
GND
Text GLabel 1700 2450 0    50   Input ~ 0
VCC
Text GLabel 10750 6300 0    50   Input ~ 0
VCC
Text GLabel 11100 6300 0    50   Input ~ 0
GND
Text Label 1700 2550 2    50   ~ 0
Data0
Text Label 2200 2550 0    50   ~ 0
Data1
Text Label 1700 2650 2    50   ~ 0
Data2
Text Label 2200 2650 0    50   ~ 0
Data3
Text Label 1700 2750 2    50   ~ 0
Data4
Text Label 2200 2750 0    50   ~ 0
Data5
Text Label 1700 2850 2    50   ~ 0
Data6
Text Label 2200 2850 0    50   ~ 0
Data7
$Comp
L Device:C C1
U 1 1 5B3105EC
P 3500 1150
F 0 "C1" H 3615 1196 50  0000 L CNN
F 1 "100n" H 3615 1105 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D10.0mm_W2.5mm_P5.00mm" H 3538 1000 50  0001 C CNN
F 3 "~" H 3500 1150 50  0001 C CNN
	1    3500 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 5B310629
P 3900 1150
F 0 "C2" H 4015 1196 50  0000 L CNN
F 1 "100n" H 4015 1105 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D10.0mm_W2.5mm_P5.00mm" H 3938 1000 50  0001 C CNN
F 3 "~" H 3900 1150 50  0001 C CNN
	1    3900 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5B310679
P 4300 1150
F 0 "C3" H 4415 1196 50  0000 L CNN
F 1 "100n" H 4415 1105 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D10.0mm_W2.5mm_P5.00mm" H 4338 1000 50  0001 C CNN
F 3 "~" H 4300 1150 50  0001 C CNN
	1    4300 1150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 5B310748
P 4700 1150
F 0 "C4" H 4815 1196 50  0000 L CNN
F 1 "100n" H 4815 1105 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D10.0mm_W2.5mm_P5.00mm" H 4738 1000 50  0001 C CNN
F 3 "~" H 4700 1150 50  0001 C CNN
	1    4700 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 1000 3900 1000
Wire Wire Line
	3900 1000 4300 1000
Connection ~ 3900 1000
Wire Wire Line
	4300 1000 4700 1000
Connection ~ 4300 1000
Wire Wire Line
	4700 1300 4300 1300
Wire Wire Line
	4300 1300 3900 1300
Connection ~ 4300 1300
Wire Wire Line
	3900 1300 3500 1300
Connection ~ 3900 1300
$Comp
L power:GND #PWR06
U 1 1 5B310956
P 3500 1300
F 0 "#PWR06" H 3500 1050 50  0001 C CNN
F 1 "GND" H 3505 1127 50  0000 C CNN
F 2 "" H 3500 1300 50  0001 C CNN
F 3 "" H 3500 1300 50  0001 C CNN
	1    3500 1300
	1    0    0    -1  
$EndComp
Connection ~ 3500 1300
$Comp
L power:VCC #PWR03
U 1 1 5B31099D
P 3500 1000
F 0 "#PWR03" H 3500 850 50  0001 C CNN
F 1 "VCC" H 3517 1173 50  0000 C CNN
F 2 "" H 3500 1000 50  0001 C CNN
F 3 "" H 3500 1000 50  0001 C CNN
	1    3500 1000
	1    0    0    -1  
$EndComp
Connection ~ 3500 1000
Text Notes 5400 1550 2    50   ~ 0
Additional capacitors to handle load spikes.
$Comp
L Connector_Generic:Conn_01x02 J7
U 1 1 5B310ABC
P 5600 1100
F 0 "J7" H 5680 1092 50  0000 L CNN
F 1 "Power_ext" H 5680 1001 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 5600 1100 50  0001 C CNN
F 3 "~" H 5600 1100 50  0001 C CNN
	1    5600 1100
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR04
U 1 1 5B310B05
P 5400 1100
F 0 "#PWR04" H 5400 950 50  0001 C CNN
F 1 "VCC" H 5417 1273 50  0000 C CNN
F 2 "" H 5400 1100 50  0001 C CNN
F 3 "" H 5400 1100 50  0001 C CNN
	1    5400 1100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 5B310B4F
P 5400 1200
F 0 "#PWR05" H 5400 950 50  0001 C CNN
F 1 "GND" H 5405 1027 50  0000 C CNN
F 2 "" H 5400 1200 50  0001 C CNN
F 3 "" H 5400 1200 50  0001 C CNN
	1    5400 1200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J1
U 1 1 5B3656B2
P 1900 2650
F 0 "J1" H 1950 3067 50  0000 C CNN
F 1 "Data_A" H 1950 2976 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 1900 2650 50  0001 C CNN
F 3 "~" H 1900 2650 50  0001 C CNN
	1    1900 2650
	1    0    0    -1  
$EndComp
Text GLabel 2200 3200 2    50   Input ~ 0
GND
Text GLabel 1700 3200 0    50   Input ~ 0
VCC
Text Label 1700 3300 2    50   ~ 0
Data0
Text Label 2200 3300 0    50   ~ 0
Data1
Text Label 1700 3400 2    50   ~ 0
Data2
Text Label 2200 3400 0    50   ~ 0
Data3
Text Label 1700 3500 2    50   ~ 0
Data4
Text Label 2200 3500 0    50   ~ 0
Data5
Text Label 1700 3600 2    50   ~ 0
Data6
Text Label 2200 3600 0    50   ~ 0
Data7
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J3
U 1 1 5B3661C7
P 1900 3400
F 0 "J3" H 1950 3817 50  0000 C CNN
F 1 "Data_C" H 1950 3726 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 1900 3400 50  0001 C CNN
F 3 "~" H 1900 3400 50  0001 C CNN
	1    1900 3400
	1    0    0    -1  
$EndComp
Text GLabel 3300 2450 2    50   Input ~ 0
GND
Text GLabel 2800 2450 0    50   Input ~ 0
VCC
Text Label 2800 2550 2    50   ~ 0
Data0
Text Label 3300 2550 0    50   ~ 0
Data1
Text Label 2800 2650 2    50   ~ 0
Data2
Text Label 3300 2650 0    50   ~ 0
Data3
Text Label 2800 2750 2    50   ~ 0
Data4
Text Label 3300 2750 0    50   ~ 0
Data5
Text Label 2800 2850 2    50   ~ 0
Data6
Text Label 3300 2850 0    50   ~ 0
Data7
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J2
U 1 1 5B36637F
P 3000 2650
F 0 "J2" H 3050 3067 50  0000 C CNN
F 1 "Data_B" H 3050 2976 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 3000 2650 50  0001 C CNN
F 3 "~" H 3000 2650 50  0001 C CNN
	1    3000 2650
	1    0    0    -1  
$EndComp
Text GLabel 3300 3200 2    50   Input ~ 0
GND
Text GLabel 2800 3200 0    50   Input ~ 0
VCC
Text Label 2800 3300 2    50   ~ 0
Data0
Text Label 3300 3300 0    50   ~ 0
Data1
Text Label 2800 3400 2    50   ~ 0
Data2
Text Label 3300 3400 0    50   ~ 0
Data3
Text Label 2800 3500 2    50   ~ 0
Data4
Text Label 3300 3500 0    50   ~ 0
Data5
Text Label 2800 3600 2    50   ~ 0
Data6
Text Label 3300 3600 0    50   ~ 0
Data7
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J4
U 1 1 5B366390
P 3000 3400
F 0 "J4" H 3050 3817 50  0000 C CNN
F 1 "Data_D" H 3050 3726 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 3000 3400 50  0001 C CNN
F 3 "~" H 3000 3400 50  0001 C CNN
	1    3000 3400
	1    0    0    -1  
$EndComp
Text GLabel 4450 2450 2    50   Input ~ 0
GND
Text GLabel 3950 2450 0    50   Input ~ 0
VCC
Text Label 3950 2550 2    50   ~ 0
Data0
Text Label 4450 2550 0    50   ~ 0
Data1
Text Label 3950 2650 2    50   ~ 0
Data2
Text Label 4450 2650 0    50   ~ 0
Data3
Text Label 3950 2750 2    50   ~ 0
Data4
Text Label 4450 2750 0    50   ~ 0
Data5
Text Label 3950 2850 2    50   ~ 0
Data6
Text Label 4450 2850 0    50   ~ 0
Data7
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J5
U 1 1 5B3664CC
P 4150 2650
F 0 "J5" H 4200 3067 50  0000 C CNN
F 1 "Conn_North" H 4200 2976 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 4150 2650 50  0001 C CNN
F 3 "~" H 4150 2650 50  0001 C CNN
	1    4150 2650
	1    0    0    -1  
$EndComp
Text GLabel 4450 3200 2    50   Input ~ 0
GND
Text GLabel 3950 3200 0    50   Input ~ 0
VCC
Text Label 3950 3300 2    50   ~ 0
Data0
Text Label 4450 3300 0    50   ~ 0
Data1
Text Label 3950 3400 2    50   ~ 0
Data2
Text Label 4450 3400 0    50   ~ 0
Data3
Text Label 3950 3500 2    50   ~ 0
Data4
Text Label 4450 3500 0    50   ~ 0
Data5
Text Label 3950 3600 2    50   ~ 0
Data6
Text Label 4450 3600 0    50   ~ 0
Data7
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J6
U 1 1 5B3664DD
P 4150 3400
F 0 "J6" H 4200 3817 50  0000 C CNN
F 1 "Conn_South" H 4200 3726 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 4150 3400 50  0001 C CNN
F 3 "~" H 4150 3400 50  0001 C CNN
	1    4150 3400
	1    0    0    -1  
$EndComp
Text GLabel 2200 3950 2    50   Input ~ 0
GND
Text GLabel 1700 3950 0    50   Input ~ 0
VCC
Text Label 1700 4050 2    50   ~ 0
Data0
Text Label 2200 4050 0    50   ~ 0
Data1
Text Label 1700 4150 2    50   ~ 0
Data2
Text Label 2200 4150 0    50   ~ 0
Data3
Text Label 1700 4250 2    50   ~ 0
Data4
Text Label 2200 4250 0    50   ~ 0
Data5
Text Label 1700 4350 2    50   ~ 0
Data6
Text Label 2200 4350 0    50   ~ 0
Data7
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J8
U 1 1 5B48E20E
P 1900 4150
F 0 "J8" H 1950 4567 50  0000 C CNN
F 1 "Data_A" H 1950 4476 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 1900 4150 50  0001 C CNN
F 3 "~" H 1900 4150 50  0001 C CNN
	1    1900 4150
	1    0    0    -1  
$EndComp
Text GLabel 2200 4700 2    50   Input ~ 0
GND
Text GLabel 1700 4700 0    50   Input ~ 0
VCC
Text Label 1700 4800 2    50   ~ 0
Data0
Text Label 2200 4800 0    50   ~ 0
Data1
Text Label 1700 4900 2    50   ~ 0
Data2
Text Label 2200 4900 0    50   ~ 0
Data3
Text Label 1700 5000 2    50   ~ 0
Data4
Text Label 2200 5000 0    50   ~ 0
Data5
Text Label 1700 5100 2    50   ~ 0
Data6
Text Label 2200 5100 0    50   ~ 0
Data7
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J10
U 1 1 5B48E21F
P 1900 4900
F 0 "J10" H 1950 5317 50  0000 C CNN
F 1 "Data_C" H 1950 5226 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 1900 4900 50  0001 C CNN
F 3 "~" H 1900 4900 50  0001 C CNN
	1    1900 4900
	1    0    0    -1  
$EndComp
Text GLabel 3300 3950 2    50   Input ~ 0
GND
Text GLabel 2800 3950 0    50   Input ~ 0
VCC
Text Label 2800 4050 2    50   ~ 0
Data0
Text Label 3300 4050 0    50   ~ 0
Data1
Text Label 2800 4150 2    50   ~ 0
Data2
Text Label 3300 4150 0    50   ~ 0
Data3
Text Label 2800 4250 2    50   ~ 0
Data4
Text Label 3300 4250 0    50   ~ 0
Data5
Text Label 2800 4350 2    50   ~ 0
Data6
Text Label 3300 4350 0    50   ~ 0
Data7
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J9
U 1 1 5B48E230
P 3000 4150
F 0 "J9" H 3050 4567 50  0000 C CNN
F 1 "Data_B" H 3050 4476 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 3000 4150 50  0001 C CNN
F 3 "~" H 3000 4150 50  0001 C CNN
	1    3000 4150
	1    0    0    -1  
$EndComp
Text GLabel 3300 4700 2    50   Input ~ 0
GND
Text GLabel 2800 4700 0    50   Input ~ 0
VCC
Text Label 2800 4800 2    50   ~ 0
Data0
Text Label 3300 4800 0    50   ~ 0
Data1
Text Label 2800 4900 2    50   ~ 0
Data2
Text Label 3300 4900 0    50   ~ 0
Data3
Text Label 2800 5000 2    50   ~ 0
Data4
Text Label 3300 5000 0    50   ~ 0
Data5
Text Label 2800 5100 2    50   ~ 0
Data6
Text Label 3300 5100 0    50   ~ 0
Data7
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J11
U 1 1 5B48E241
P 3000 4900
F 0 "J11" H 3050 5317 50  0000 C CNN
F 1 "Data_D" H 3050 5226 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 3000 4900 50  0001 C CNN
F 3 "~" H 3000 4900 50  0001 C CNN
	1    3000 4900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 5B48E9B8
P 3500 1900
F 0 "C5" H 3615 1946 50  0000 L CNN
F 1 "100n" H 3615 1855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D10.0mm_W2.5mm_P5.00mm" H 3538 1750 50  0001 C CNN
F 3 "~" H 3500 1900 50  0001 C CNN
	1    3500 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C6
U 1 1 5B48E9BF
P 3900 1900
F 0 "C6" H 4015 1946 50  0000 L CNN
F 1 "100n" H 4015 1855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D10.0mm_W2.5mm_P5.00mm" H 3938 1750 50  0001 C CNN
F 3 "~" H 3900 1900 50  0001 C CNN
	1    3900 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C7
U 1 1 5B48E9C6
P 4300 1900
F 0 "C7" H 4415 1946 50  0000 L CNN
F 1 "100n" H 4415 1855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D10.0mm_W2.5mm_P5.00mm" H 4338 1750 50  0001 C CNN
F 3 "~" H 4300 1900 50  0001 C CNN
	1    4300 1900
	1    0    0    -1  
$EndComp
$Comp
L Device:C C8
U 1 1 5B48E9CD
P 4700 1900
F 0 "C8" H 4815 1946 50  0000 L CNN
F 1 "100n" H 4815 1855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D10.0mm_W2.5mm_P5.00mm" H 4738 1750 50  0001 C CNN
F 3 "~" H 4700 1900 50  0001 C CNN
	1    4700 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 1750 3900 1750
Wire Wire Line
	3900 1750 4300 1750
Connection ~ 3900 1750
Wire Wire Line
	4300 1750 4700 1750
Connection ~ 4300 1750
Wire Wire Line
	4700 2050 4300 2050
Wire Wire Line
	4300 2050 3900 2050
Connection ~ 4300 2050
Wire Wire Line
	3900 2050 3500 2050
Connection ~ 3900 2050
$Comp
L power:GND #PWR08
U 1 1 5B48E9DE
P 3500 2050
F 0 "#PWR08" H 3500 1800 50  0001 C CNN
F 1 "GND" H 3505 1877 50  0000 C CNN
F 2 "" H 3500 2050 50  0001 C CNN
F 3 "" H 3500 2050 50  0001 C CNN
	1    3500 2050
	1    0    0    -1  
$EndComp
Connection ~ 3500 2050
$Comp
L power:VCC #PWR07
U 1 1 5B48E9E5
P 3500 1750
F 0 "#PWR07" H 3500 1600 50  0001 C CNN
F 1 "VCC" H 3517 1923 50  0000 C CNN
F 2 "" H 3500 1750 50  0001 C CNN
F 3 "" H 3500 1750 50  0001 C CNN
	1    3500 1750
	1    0    0    -1  
$EndComp
Connection ~ 3500 1750
$EndSCHEMATC
