EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J1
U 1 1 5B3B8FD5
P 1500 1200
F 0 "J1" H 1550 1617 50  0000 C CNN
F 1 "Argument_A" H 1550 1526 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 1500 1200 50  0001 C CNN
F 3 "~" H 1500 1200 50  0001 C CNN
	1    1500 1200
	1    0    0    -1  
$EndComp
Text GLabel 1300 1000 0    50   Input ~ 0
VCC
Text GLabel 1800 1000 2    50   Input ~ 0
GND
Text Label 1300 1100 2    50   ~ 0
A0
Text Label 1800 1100 0    50   ~ 0
A1
Text Label 1300 1200 2    50   ~ 0
A2
Text Label 1800 1200 0    50   ~ 0
A3
Text Label 1300 1300 2    50   ~ 0
A4
Text Label 1800 1300 0    50   ~ 0
A5
Text Label 1300 1400 2    50   ~ 0
A6
Text Label 1800 1400 0    50   ~ 0
A7
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J3
U 1 1 5B3B92A5
P 2950 1200
F 0 "J3" H 3000 1617 50  0000 C CNN
F 1 "A and B" H 3000 1526 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 2950 1200 50  0001 C CNN
F 3 "~" H 2950 1200 50  0001 C CNN
	1    2950 1200
	1    0    0    -1  
$EndComp
Text GLabel 2750 1000 0    50   Input ~ 0
VCC
Text GLabel 3250 1000 2    50   Input ~ 0
GND
Text Label 2750 1100 2    50   ~ 0
And0
Text Label 3250 1100 0    50   ~ 0
And1
Text Label 2750 1200 2    50   ~ 0
And2
Text Label 3250 1200 0    50   ~ 0
And3
Text Label 2750 1300 2    50   ~ 0
And4
Text Label 3250 1300 0    50   ~ 0
And5
Text Label 2750 1400 2    50   ~ 0
And6
Text Label 3250 1400 0    50   ~ 0
And7
Text Label 6500 950  2    50   ~ 0
A0
Text Label 6500 1350 2    50   ~ 0
A1
Text Label 6500 1700 2    50   ~ 0
A2
Text Label 6500 2050 2    50   ~ 0
A3
Text Label 6500 2600 2    50   ~ 0
A4
Text Label 6500 3000 2    50   ~ 0
A5
Text Label 6500 3400 2    50   ~ 0
A6
Text Label 6500 3800 2    50   ~ 0
A7
Wire Wire Line
	1000 3050 1450 3050
Wire Wire Line
	1000 4050 1450 4050
$Comp
L power:GND #PWR0101
U 1 1 5B3BC05B
P 1000 4050
F 0 "#PWR0101" H 1000 3800 50  0001 C CNN
F 1 "GND" H 1005 3877 50  0000 C CNN
F 2 "" H 1000 4050 50  0001 C CNN
F 3 "" H 1000 4050 50  0001 C CNN
	1    1000 4050
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0102
U 1 1 5B3BC0D8
P 1000 3050
F 0 "#PWR0102" H 1000 2900 50  0001 C CNN
F 1 "VCC" H 1017 3223 50  0000 C CNN
F 2 "" H 1000 3050 50  0001 C CNN
F 3 "" H 1000 3050 50  0001 C CNN
	1    1000 3050
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0103
U 1 1 5B3BC14E
P 1000 4750
F 0 "#PWR0103" H 1000 4600 50  0001 C CNN
F 1 "VCC" H 1017 4923 50  0000 C CNN
F 2 "" H 1000 4750 50  0001 C CNN
F 3 "" H 1000 4750 50  0001 C CNN
	1    1000 4750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5B3BC1CB
P 1400 4750
F 0 "#PWR0104" H 1400 4500 50  0001 C CNN
F 1 "GND" H 1405 4577 50  0000 C CNN
F 2 "" H 1400 4750 50  0001 C CNN
F 3 "" H 1400 4750 50  0001 C CNN
	1    1400 4750
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5B3BC248
P 1400 4750
F 0 "#FLG0101" H 1400 4825 50  0001 C CNN
F 1 "PWR_FLAG" H 1400 4924 50  0000 C CNN
F 2 "" H 1400 4750 50  0001 C CNN
F 3 "~" H 1400 4750 50  0001 C CNN
	1    1400 4750
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5B3BC2BE
P 1000 4750
F 0 "#FLG0102" H 1000 4825 50  0001 C CNN
F 1 "PWR_FLAG" H 1000 4923 50  0000 C CNN
F 2 "" H 1000 4750 50  0001 C CNN
F 3 "~" H 1000 4750 50  0001 C CNN
	1    1000 4750
	-1   0    0    1   
$EndComp
Text GLabel 1000 4750 0    50   Input ~ 0
VCC
Text GLabel 1400 4750 2    50   Input ~ 0
GND
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J2
U 1 1 5B48BDD6
P 1500 1950
F 0 "J2" H 1550 2367 50  0000 C CNN
F 1 "Argument_B" H 1550 2276 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 1500 1950 50  0001 C CNN
F 3 "~" H 1500 1950 50  0001 C CNN
	1    1500 1950
	1    0    0    -1  
$EndComp
Text GLabel 1300 1750 0    50   Input ~ 0
VCC
Text GLabel 1800 1750 2    50   Input ~ 0
GND
Text Label 1300 1850 2    50   ~ 0
B0
Text Label 1800 1850 0    50   ~ 0
B1
Text Label 1300 1950 2    50   ~ 0
B2
Text Label 1800 1950 0    50   ~ 0
B3
Text Label 1300 2050 2    50   ~ 0
B4
Text Label 1800 2050 0    50   ~ 0
B5
Text Label 1300 2150 2    50   ~ 0
B6
Text Label 1800 2150 0    50   ~ 0
B7
$Comp
L 74xx:74HC00 U1
U 1 1 5B48C2C6
P 6800 1050
F 0 "U1" H 6800 1375 50  0000 C CNN
F 1 "74HC00" H 6800 1284 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 6800 1050 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 6800 1050 50  0001 C CNN
	1    6800 1050
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U1
U 2 1 5B48C39A
P 6800 1450
F 0 "U1" H 6800 1775 50  0000 C CNN
F 1 "74HC00" H 6800 1684 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 6800 1450 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 6800 1450 50  0001 C CNN
	2    6800 1450
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U1
U 3 1 5B48C412
P 6800 1800
F 0 "U1" H 6800 2125 50  0000 C CNN
F 1 "74HC00" H 6800 2034 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 6800 1800 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 6800 1800 50  0001 C CNN
	3    6800 1800
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U1
U 4 1 5B48C475
P 6800 2150
F 0 "U1" H 6800 2475 50  0000 C CNN
F 1 "74HC00" H 6800 2384 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 6800 2150 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 6800 2150 50  0001 C CNN
	4    6800 2150
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U1
U 5 1 5B48C4C7
P 1000 3550
F 0 "U1" H 1230 3596 50  0000 L CNN
F 1 "74HC00" H 1230 3505 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 1000 3550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 1000 3550 50  0001 C CNN
	5    1000 3550
	1    0    0    -1  
$EndComp
Connection ~ 1000 3050
Connection ~ 1000 4050
$Comp
L 74xx:74HC00 U2
U 1 1 5B48C5FB
P 6800 2700
F 0 "U2" H 6800 3025 50  0000 C CNN
F 1 "74HC00" H 6800 2934 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 6800 2700 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 6800 2700 50  0001 C CNN
	1    6800 2700
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U2
U 2 1 5B48C7D1
P 6800 3100
F 0 "U2" H 6800 3425 50  0000 C CNN
F 1 "74HC00" H 6800 3334 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 6800 3100 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 6800 3100 50  0001 C CNN
	2    6800 3100
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U2
U 3 1 5B48C83A
P 6800 3500
F 0 "U2" H 6800 3825 50  0000 C CNN
F 1 "74HC00" H 6800 3734 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 6800 3500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 6800 3500 50  0001 C CNN
	3    6800 3500
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U2
U 4 1 5B48C8A4
P 6800 3900
F 0 "U2" H 6800 4225 50  0000 C CNN
F 1 "74HC00" H 6800 4134 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 6800 3900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 6800 3900 50  0001 C CNN
	4    6800 3900
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74HC00 U2
U 5 1 5B48C935
P 1450 3550
F 0 "U2" H 1680 3596 50  0000 L CNN
F 1 "74HC00" H 1680 3505 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 1450 3550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74hc00" H 1450 3550 50  0001 C CNN
	5    1450 3550
	1    0    0    -1  
$EndComp
Text Label 6500 1150 2    50   ~ 0
B0
Text Label 6500 1550 2    50   ~ 0
B1
Text Label 6500 1900 2    50   ~ 0
B2
Text Label 6500 2250 2    50   ~ 0
B3
Text Label 6500 2800 2    50   ~ 0
B4
Text Label 6500 3200 2    50   ~ 0
B5
Text Label 6500 3600 2    50   ~ 0
B6
Text Label 6500 4000 2    50   ~ 0
B7
Text Label 7100 1050 0    50   ~ 0
And0
Text Label 7100 1450 0    50   ~ 0
And1
Text Label 7100 1800 0    50   ~ 0
And2
Text Label 7100 2150 0    50   ~ 0
And3
Text Label 7100 2700 0    50   ~ 0
And4
Text Label 7100 3100 0    50   ~ 0
And5
Text Label 7100 3500 0    50   ~ 0
And6
Text Label 7100 3900 0    50   ~ 0
And7
Text Notes 7600 1250 0    50   ~ 0
Compatible with \n* 7408 (AND)\n* 7432 (OR)\nNOT compatible with \n* 7402 (NOR)
$EndSCHEMATC
