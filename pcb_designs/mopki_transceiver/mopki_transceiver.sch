EESchema Schematic File Version 4
LIBS:mopki_transceiver-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L 74xx:74HC245 U1
U 1 1 5B1A8308
P 3050 1500
F 0 "U1" H 2850 2150 50  0000 C CNN
F 1 "74HC245" H 3250 2150 50  0000 C CNN
F 2 "Package_DIP:DIP-20_W7.62mm" H 3050 1500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74HC245" H 3050 1500 50  0001 C CNN
	1    3050 1500
	1    0    0    -1  
$EndComp
Text Label 2550 1000 2    50   ~ 0
DataIn0
Text Label 2550 1100 2    50   ~ 0
DataIn1
Text Label 2550 1200 2    50   ~ 0
DataIn2
Text Label 2550 1300 2    50   ~ 0
DataIn3
Text Label 2550 1400 2    50   ~ 0
DataIn4
Text Label 2550 1500 2    50   ~ 0
DataIn5
Text Label 2550 1600 2    50   ~ 0
DataIn6
Text Label 2550 1700 2    50   ~ 0
DataIn7
Text Label 3550 1000 0    50   ~ 0
DataOut0
Text Label 3550 1100 0    50   ~ 0
DataOut1
Text Label 3550 1200 0    50   ~ 0
DataOut2
Text Label 3550 1300 0    50   ~ 0
DataOut3
Text Label 3550 1400 0    50   ~ 0
DataOut4
Text Label 3550 1500 0    50   ~ 0
DataOut5
Text Label 3550 1600 0    50   ~ 0
DataOut6
Text Label 3550 1700 0    50   ~ 0
DataOut7
Text Label 2550 2000 2    50   ~ 0
-CE
Text Label 2550 1900 2    50   ~ 0
Direction
$Comp
L power:GND #PWR02
U 1 1 5B1A8512
P 3050 2300
F 0 "#PWR02" H 3050 2050 50  0001 C CNN
F 1 "GND" H 3055 2127 50  0000 C CNN
F 2 "" H 3050 2300 50  0001 C CNN
F 3 "" H 3050 2300 50  0001 C CNN
	1    3050 2300
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR01
U 1 1 5B1A8547
P 3050 700
F 0 "#PWR01" H 3050 550 50  0001 C CNN
F 1 "VCC" H 3067 873 50  0000 C CNN
F 2 "" H 3050 700 50  0001 C CNN
F 3 "" H 3050 700 50  0001 C CNN
	1    3050 700 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5B1951F8
P 11100 6300
F 0 "#PWR07" H 11100 6050 50  0001 C CNN
F 1 "GND" H 11105 6127 50  0000 C CNN
F 2 "" H 11100 6300 50  0001 C CNN
F 3 "" H 11100 6300 50  0001 C CNN
	1    11100 6300
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR05
U 1 1 5B1952DF
P 10750 6300
F 0 "#PWR05" H 10750 6150 50  0001 C CNN
F 1 "VCC" H 10767 6473 50  0000 C CNN
F 2 "" H 10750 6300 50  0001 C CNN
F 3 "" H 10750 6300 50  0001 C CNN
	1    10750 6300
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG03
U 1 1 5B19534E
P 11100 6300
F 0 "#FLG03" H 11100 6375 50  0001 C CNN
F 1 "PWR_FLAG" H 11100 6474 50  0000 C CNN
F 2 "" H 11100 6300 50  0001 C CNN
F 3 "~" H 11100 6300 50  0001 C CNN
	1    11100 6300
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5B195370
P 10750 6300
F 0 "#FLG01" H 10750 6375 50  0001 C CNN
F 1 "PWR_FLAG" H 10750 6473 50  0000 C CNN
F 2 "" H 10750 6300 50  0001 C CNN
F 3 "~" H 10750 6300 50  0001 C CNN
	1    10750 6300
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J1
U 1 1 5B195462
P 1100 1350
F 0 "J1" H 1150 1800 50  0000 C CNN
F 1 "DataIn" H 1150 1700 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 1100 1350 50  0001 C CNN
F 3 "~" H 1100 1350 50  0001 C CNN
	1    1100 1350
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J2
U 1 1 5B34E982
P 1100 2100
F 0 "J2" H 1150 2500 50  0000 C CNN
F 1 "DataOut" H 1150 2400 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 1100 2100 50  0001 C CNN
F 3 "~" H 1100 2100 50  0001 C CNN
	1    1100 2100
	1    0    0    -1  
$EndComp
Text Label 900  1250 2    50   ~ 0
DataIn0
Text Label 1400 1250 0    50   ~ 0
DataIn1
Text Label 900  1350 2    50   ~ 0
DataIn2
Text Label 1400 1350 0    50   ~ 0
DataIn3
Text Label 900  1450 2    50   ~ 0
DataIn4
Text Label 1400 1450 0    50   ~ 0
DataIn5
Text Label 900  1550 2    50   ~ 0
DataIn6
Text Label 1400 1550 0    50   ~ 0
DataIn7
Text Label 900  2000 2    50   ~ 0
DataOut0
Text Label 1400 2000 0    50   ~ 0
DataOut1
Text Label 900  2100 2    50   ~ 0
DataOut2
Text Label 1400 2100 0    50   ~ 0
DataOut3
Text Label 900  2200 2    50   ~ 0
DataOut4
Text Label 1400 2200 0    50   ~ 0
DataOut5
Text Label 900  2300 2    50   ~ 0
DataOut6
Text Label 1400 2300 0    50   ~ 0
DataOut7
Text GLabel 1400 1900 2    50   Input ~ 0
GND
Text GLabel 1400 1150 2    50   Input ~ 0
GND
Text GLabel 900  1150 0    50   Input ~ 0
VCC
Text GLabel 900  1900 0    50   Input ~ 0
VCC
$Comp
L Connector:Conn_01x02_Male J3
U 1 1 5B34EB3E
P 1050 2850
F 0 "J3" H 1156 3028 50  0000 C CNN
F 1 "Conn_01x02_Male" H 1156 2937 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1050 2850 50  0001 C CNN
F 3 "~" H 1050 2850 50  0001 C CNN
	1    1050 2850
	1    0    0    -1  
$EndComp
Text Label 1250 2950 0    50   ~ 0
CE
Text Label 1250 2850 0    50   ~ 0
Direction
Text GLabel 11100 6300 0    50   Input ~ 0
GND
Text GLabel 10750 6300 0    50   Input ~ 0
VCC
$Comp
L 74xx:74LS04 U2
U 1 1 5B3E33BD
P 2500 5300
F 0 "U2" H 2500 5617 50  0000 C CNN
F 1 "74LS04" H 2500 5526 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 2500 5300 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 2500 5300 50  0001 C CNN
	1    2500 5300
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U2
U 2 1 5B3E348D
P 2500 4350
F 0 "U2" H 2500 4667 50  0000 C CNN
F 1 "74LS04" H 2500 4576 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 2500 4350 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 2500 4350 50  0001 C CNN
	2    2500 4350
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U2
U 3 1 5B3E3500
P 2500 4600
F 0 "U2" H 2500 4917 50  0000 C CNN
F 1 "74LS04" H 2500 4826 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 2500 4600 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 2500 4600 50  0001 C CNN
	3    2500 4600
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U2
U 4 1 5B3E359E
P 2500 4850
F 0 "U2" H 2500 5167 50  0000 C CNN
F 1 "74LS04" H 2500 5076 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 2500 4850 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 2500 4850 50  0001 C CNN
	4    2500 4850
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U2
U 5 1 5B3E3633
P 2500 5050
F 0 "U2" H 2500 5367 50  0000 C CNN
F 1 "74LS04" H 2500 5276 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 2500 5050 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 2500 5050 50  0001 C CNN
	5    2500 5050
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U2
U 6 1 5B3E36FC
P 2500 3900
F 0 "U2" H 2500 4217 50  0000 C CNN
F 1 "74LS04" H 2500 4126 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 2500 3900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 2500 3900 50  0001 C CNN
	6    2500 3900
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U2
U 7 1 5B3E377F
P 1000 4700
F 0 "U2" H 1230 4746 50  0000 L CNN
F 1 "74LS04" H 1230 4655 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 1000 4700 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 1000 4700 50  0001 C CNN
	7    1000 4700
	1    0    0    -1  
$EndComp
NoConn ~ 2200 5300
NoConn ~ 2200 5050
NoConn ~ 2200 4850
NoConn ~ 2200 4600
NoConn ~ 2200 4350
NoConn ~ 2800 4350
NoConn ~ 2800 4600
NoConn ~ 2800 4850
NoConn ~ 2800 5050
NoConn ~ 2800 5300
$Comp
L power:GND #PWR04
U 1 1 5B3E3AE0
P 1000 5200
F 0 "#PWR04" H 1000 4950 50  0001 C CNN
F 1 "GND" H 1005 5027 50  0000 C CNN
F 2 "" H 1000 5200 50  0001 C CNN
F 3 "" H 1000 5200 50  0001 C CNN
	1    1000 5200
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR03
U 1 1 5B3E3B75
P 1000 4200
F 0 "#PWR03" H 1000 4050 50  0001 C CNN
F 1 "VCC" H 1017 4373 50  0000 C CNN
F 2 "" H 1000 4200 50  0001 C CNN
F 3 "" H 1000 4200 50  0001 C CNN
	1    1000 4200
	1    0    0    -1  
$EndComp
Text Label 2200 3900 2    50   ~ 0
CE
Text Label 2800 3900 0    50   ~ 0
-CE
Text Notes 3500 2450 0    50   ~ 0
Direction determines the transceiver direction:\nLOW:  DataOut -> DataIn\nHIGH: DataIn  -> DataOut
$EndSCHEMATC
