EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J1
U 1 1 5B3B72E0
P 1600 1300
F 0 "J1" H 1650 1717 50  0000 C CNN
F 1 "Initial_Value" H 1650 1626 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 1600 1300 50  0001 C CNN
F 3 "~" H 1600 1300 50  0001 C CNN
	1    1600 1300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR07
U 1 1 5B3B7373
P 10100 6300
F 0 "#PWR07" H 10100 6050 50  0001 C CNN
F 1 "GND" H 10105 6127 50  0000 C CNN
F 2 "" H 10100 6300 50  0001 C CNN
F 3 "" H 10100 6300 50  0001 C CNN
	1    10100 6300
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR08
U 1 1 5B3B73DA
P 10500 6300
F 0 "#PWR08" H 10500 6150 50  0001 C CNN
F 1 "VCC" H 10517 6473 50  0000 C CNN
F 2 "" H 10500 6300 50  0001 C CNN
F 3 "" H 10500 6300 50  0001 C CNN
	1    10500 6300
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5B3B743E
P 10100 6300
F 0 "#FLG01" H 10100 6375 50  0001 C CNN
F 1 "PWR_FLAG" H 10100 6474 50  0000 C CNN
F 2 "" H 10100 6300 50  0001 C CNN
F 3 "~" H 10100 6300 50  0001 C CNN
	1    10100 6300
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5B3B7463
P 10500 6300
F 0 "#FLG02" H 10500 6375 50  0001 C CNN
F 1 "PWR_FLAG" H 10500 6473 50  0000 C CNN
F 2 "" H 10500 6300 50  0001 C CNN
F 3 "~" H 10500 6300 50  0001 C CNN
	1    10500 6300
	-1   0    0    1   
$EndComp
Text GLabel 10100 6300 0    50   Input ~ 0
GND
Text GLabel 10500 6300 0    50   Input ~ 0
VCC
Text GLabel 1400 1100 0    50   Input ~ 0
VCC
Text GLabel 1900 1100 2    50   Input ~ 0
GND
Text Label 1400 1200 2    50   ~ 0
Initial0
Text Label 1900 1200 0    50   ~ 0
Initial1
Text Label 1400 1300 2    50   ~ 0
Initial2
Text Label 1900 1300 0    50   ~ 0
Initial3
Text Label 1400 1400 2    50   ~ 0
Initial4
Text Label 1900 1400 0    50   ~ 0
Initial5
Text Label 1400 1500 2    50   ~ 0
Initial6
Text Label 1900 1500 0    50   ~ 0
Initial7
$Comp
L 74xx:74LS161 U1
U 1 1 5B3B7620
P 4450 1550
F 0 "U1" H 4200 2200 50  0000 C CNN
F 1 "74LS161" H 4700 2200 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 4450 1550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS161" H 4450 1550 50  0001 C CNN
	1    4450 1550
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR01
U 1 1 5B3B76F1
P 4450 750
F 0 "#PWR01" H 4450 600 50  0001 C CNN
F 1 "VCC" H 4467 923 50  0000 C CNN
F 2 "" H 4450 750 50  0001 C CNN
F 3 "" H 4450 750 50  0001 C CNN
	1    4450 750 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 5B3B7728
P 4450 2350
F 0 "#PWR02" H 4450 2100 50  0001 C CNN
F 1 "GND" H 4455 2177 50  0000 C CNN
F 2 "" H 4450 2350 50  0001 C CNN
F 3 "" H 4450 2350 50  0001 C CNN
	1    4450 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 1650 3950 1750
Wire Wire Line
	3950 1750 3700 1750
Connection ~ 3950 1750
Text Label 3700 1750 2    50   ~ 0
Count_Enable
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J2
U 1 1 5B3B77FB
P 7250 1150
F 0 "J2" H 7300 1567 50  0000 C CNN
F 1 "Counter_Value" H 7300 1476 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 7250 1150 50  0001 C CNN
F 3 "~" H 7250 1150 50  0001 C CNN
	1    7250 1150
	1    0    0    -1  
$EndComp
Text GLabel 7050 950  0    50   Input ~ 0
VCC
Text GLabel 7550 950  2    50   Input ~ 0
GND
Text Label 7050 1050 2    50   ~ 0
Counter_Value0
Text Label 7550 1050 0    50   ~ 0
Counter_Value1
Text Label 7050 1150 2    50   ~ 0
Counter_Value2
Text Label 7550 1150 0    50   ~ 0
Counter_Value3
Text Label 7050 1250 2    50   ~ 0
Counter_Value4
Text Label 7550 1250 0    50   ~ 0
Counter_Value5
Text Label 7050 1350 2    50   ~ 0
Counter_Value6
Text Label 7550 1350 0    50   ~ 0
Counter_Value7
Text Label 4950 1050 0    50   ~ 0
Counter_Value0
Text Label 4950 1150 0    50   ~ 0
Counter_Value1
Text Label 4950 1250 0    50   ~ 0
Counter_Value2
Text Label 4950 1350 0    50   ~ 0
Counter_Value3
Text Label 4950 3100 0    50   ~ 0
Counter_Value4
Text Label 4950 3200 0    50   ~ 0
Counter_Value5
Text Label 4950 3300 0    50   ~ 0
Counter_Value6
Text Label 4950 3400 0    50   ~ 0
Counter_Value7
$Comp
L 74xx:74LS161 U3
U 1 1 5B3B7A1A
P 4450 3600
F 0 "U3" H 4200 4250 50  0000 C CNN
F 1 "74LS161" H 4700 4250 50  0000 C CNN
F 2 "Package_DIP:DIP-16_W7.62mm" H 4450 3600 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS161" H 4450 3600 50  0001 C CNN
	1    4450 3600
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR03
U 1 1 5B3B7A21
P 4450 2800
F 0 "#PWR03" H 4450 2650 50  0001 C CNN
F 1 "VCC" H 4467 2973 50  0000 C CNN
F 2 "" H 4450 2800 50  0001 C CNN
F 3 "" H 4450 2800 50  0001 C CNN
	1    4450 2800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5B3B7A27
P 4450 4400
F 0 "#PWR04" H 4450 4150 50  0001 C CNN
F 1 "GND" H 4455 4227 50  0000 C CNN
F 2 "" H 4450 4400 50  0001 C CNN
F 3 "" H 4450 4400 50  0001 C CNN
	1    4450 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3950 3700 3950 3800
Wire Wire Line
	3950 3800 3700 3800
Connection ~ 3950 3800
Text Label 3700 3800 2    50   ~ 0
Count_Enable
Text Label 3950 2050 2    50   ~ 0
-Reset
Text Label 3950 4100 2    50   ~ 0
-Reset
Text Label 3950 1050 2    50   ~ 0
Initial0
Text Label 3950 1150 2    50   ~ 0
Initial1
Text Label 3950 1250 2    50   ~ 0
Initial2
Text Label 3950 1350 2    50   ~ 0
Initial3
Text Label 3950 3100 2    50   ~ 0
Initial4
Text Label 3950 3200 2    50   ~ 0
Initial5
Text Label 3950 3300 2    50   ~ 0
Initial6
Text Label 3950 3400 2    50   ~ 0
Initial7
Text Label 3950 1550 2    50   ~ 0
-Load
Text Label 3950 3600 2    50   ~ 0
-Load
Text Label 3950 1850 2    50   ~ 0
Pulse
Wire Wire Line
	3950 3900 3050 3900
Wire Wire Line
	3050 3900 3050 2600
Wire Wire Line
	3050 2600 4950 2600
Wire Wire Line
	4950 2600 4950 1550
Text Label 4950 3600 0    50   ~ 0
Overflow
$Comp
L Connector:Conn_01x04_Male J3
U 1 1 5B3B7C66
P 1550 2200
F 0 "J3" H 1656 2478 50  0000 C CNN
F 1 "Conn_01x04_Male" H 1656 2387 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 1550 2200 50  0001 C CNN
F 3 "~" H 1550 2200 50  0001 C CNN
	1    1550 2200
	1    0    0    -1  
$EndComp
Text Label 1750 2300 0    50   ~ 0
Load
Text Label 1750 2200 0    50   ~ 0
Pulse
Text Label 1750 2100 0    50   ~ 0
Count_Enable
Text Label 1750 2400 0    50   ~ 0
Reset
$Comp
L 74xx:74LS04 U2
U 1 1 5B3B7D7A
P 1600 3300
F 0 "U2" H 1600 3617 50  0000 C CNN
F 1 "74LS04" H 1600 3526 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 1600 3300 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 1600 3300 50  0001 C CNN
	1    1600 3300
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U2
U 2 1 5B3B7E55
P 1600 3550
F 0 "U2" H 1600 3867 50  0000 C CNN
F 1 "74LS04" H 1600 3776 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 1600 3550 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 1600 3550 50  0001 C CNN
	2    1600 3550
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U2
U 3 1 5B3B7F28
P 1600 3750
F 0 "U2" H 1600 4067 50  0000 C CNN
F 1 "74LS04" H 1600 3976 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 1600 3750 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 1600 3750 50  0001 C CNN
	3    1600 3750
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U2
U 4 1 5B3B7F85
P 1600 3950
F 0 "U2" H 1600 4267 50  0000 C CNN
F 1 "74LS04" H 1600 4176 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 1600 3950 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 1600 3950 50  0001 C CNN
	4    1600 3950
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U2
U 5 1 5B3B7FE3
P 1600 4150
F 0 "U2" H 1600 4467 50  0000 C CNN
F 1 "74LS04" H 1600 4376 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 1600 4150 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 1600 4150 50  0001 C CNN
	5    1600 4150
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U2
U 6 1 5B3B8046
P 1600 4350
F 0 "U2" H 1600 4667 50  0000 C CNN
F 1 "74LS04" H 1600 4576 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 1600 4350 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 1600 4350 50  0001 C CNN
	6    1600 4350
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U2
U 7 1 5B3B80A2
P 900 5000
F 0 "U2" H 1130 5046 50  0000 L CNN
F 1 "74LS04" H 1130 4955 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 900 5000 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 900 5000 50  0001 C CNN
	7    900  5000
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR05
U 1 1 5B3B8159
P 900 4500
F 0 "#PWR05" H 900 4350 50  0001 C CNN
F 1 "VCC" H 917 4673 50  0000 C CNN
F 2 "" H 900 4500 50  0001 C CNN
F 3 "" H 900 4500 50  0001 C CNN
	1    900  4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 5B3B81A6
P 900 5500
F 0 "#PWR06" H 900 5250 50  0001 C CNN
F 1 "GND" H 905 5327 50  0000 C CNN
F 2 "" H 900 5500 50  0001 C CNN
F 3 "" H 900 5500 50  0001 C CNN
	1    900  5500
	1    0    0    -1  
$EndComp
Text Label 1300 3300 2    50   ~ 0
Reset
Text Label 1900 3300 0    50   ~ 0
-Reset
Text Label 1300 3550 2    50   ~ 0
Load
Text Label 1900 3550 0    50   ~ 0
-Load
NoConn ~ 1300 3750
NoConn ~ 1300 3950
NoConn ~ 1300 4150
NoConn ~ 1300 4350
NoConn ~ 1900 4350
NoConn ~ 1900 4150
NoConn ~ 1900 3950
NoConn ~ 1900 3750
$Comp
L Connector:Conn_01x01_Male J4
U 1 1 5B3B86CC
P 7450 1700
F 0 "J4" H 7423 1630 50  0000 R CNN
F 1 "Conn_01x01_Male" H 7423 1721 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 7450 1700 50  0001 C CNN
F 3 "~" H 7450 1700 50  0001 C CNN
	1    7450 1700
	-1   0    0    1   
$EndComp
Text Label 7250 1700 2    50   ~ 0
Overflow
$EndSCHEMATC
