EESchema Schematic File Version 4
LIBS:mopki_rom-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR02
U 1 1 5B1951F8
P 2600 4850
F 0 "#PWR02" H 2600 4600 50  0001 C CNN
F 1 "GND" H 2605 4677 50  0000 C CNN
F 2 "" H 2600 4850 50  0001 C CNN
F 3 "" H 2600 4850 50  0001 C CNN
	1    2600 4850
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR01
U 1 1 5B1952DF
P 2250 4850
F 0 "#PWR01" H 2250 4700 50  0001 C CNN
F 1 "VCC" H 2267 5023 50  0000 C CNN
F 2 "" H 2250 4850 50  0001 C CNN
F 3 "" H 2250 4850 50  0001 C CNN
	1    2250 4850
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5B19534E
P 2600 4850
F 0 "#FLG02" H 2600 4925 50  0001 C CNN
F 1 "PWR_FLAG" H 2600 5024 50  0000 C CNN
F 2 "" H 2600 4850 50  0001 C CNN
F 3 "~" H 2600 4850 50  0001 C CNN
	1    2600 4850
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5B195370
P 2250 4850
F 0 "#FLG01" H 2250 4925 50  0001 C CNN
F 1 "PWR_FLAG" H 2250 5023 50  0000 C CNN
F 2 "" H 2250 4850 50  0001 C CNN
F 3 "~" H 2250 4850 50  0001 C CNN
	1    2250 4850
	-1   0    0    1   
$EndComp
Text GLabel 1450 2400 2    50   Input ~ 0
GND
Text GLabel 950  2400 0    50   Input ~ 0
VCC
Text GLabel 2250 4850 0    50   Input ~ 0
VCC
Text GLabel 2600 4850 0    50   Input ~ 0
GND
Text Label 950  1750 2    50   ~ 0
Addr_High0
Text Label 1450 1750 0    50   ~ 0
Addr_High1
Text Label 950  1850 2    50   ~ 0
Addr_High2
Text Label 1450 1850 0    50   ~ 0
Addr_High3
Text Label 950  1950 2    50   ~ 0
Addr_High4
Text Label 1450 1950 0    50   ~ 0
Addr_High5
Text Label 950  2050 2    50   ~ 0
Addr_High6
Text Label 1450 2050 0    50   ~ 0
Addr_High7
$Comp
L power:GND #PWR0101
U 1 1 5B27C872
P 3400 3600
F 0 "#PWR0101" H 3400 3350 50  0001 C CNN
F 1 "GND" H 3405 3427 50  0000 C CNN
F 2 "" H 3400 3600 50  0001 C CNN
F 3 "" H 3400 3600 50  0001 C CNN
	1    3400 3600
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0102
U 1 1 5B27C944
P 3400 1400
F 0 "#PWR0102" H 3400 1250 50  0001 C CNN
F 1 "VCC" H 3417 1573 50  0000 C CNN
F 2 "" H 3400 1400 50  0001 C CNN
F 3 "" H 3400 1400 50  0001 C CNN
	1    3400 1400
	1    0    0    -1  
$EndComp
Text Label 3000 3200 2    50   ~ 0
-IE
Text Label 3000 3300 2    50   ~ 0
-OE
Text Label 3000 3400 2    50   ~ 0
-CE
Text Label 3000 1800 2    50   ~ 0
Addr_Low0
Text Label 3000 1900 2    50   ~ 0
Addr_Low1
Text Label 3000 2000 2    50   ~ 0
Addr_Low2
Text Label 3000 2100 2    50   ~ 0
Addr_Low3
Text Label 3000 2200 2    50   ~ 0
Addr_Low4
Text Label 3000 2300 2    50   ~ 0
Addr_Low5
Text Label 3000 2800 2    50   ~ 0
Addr_Low6
Text Label 3000 3000 2    50   ~ 0
Addr_Low7
Text Label 3000 2900 2    50   ~ 0
Addr_High0
Text Label 3000 2400 2    50   ~ 0
Addr_High1
Text Label 3000 2500 2    50   ~ 0
Addr_High2
Text Label 3000 2700 2    50   ~ 0
Addr_High3
Text Label 3000 2600 2    50   ~ 0
Addr_High4
Text Label 3000 1700 2    50   ~ 0
Addr_High5
Text Label 3000 1600 2    50   ~ 0
Addr_High6
Text Label 3800 1600 0    50   ~ 0
Data0
Text Label 3800 1700 0    50   ~ 0
Data1
Text Label 3800 1800 0    50   ~ 0
Data2
Text Label 3800 1900 0    50   ~ 0
Data3
Text Label 3800 2000 0    50   ~ 0
Data4
Text Label 3800 2100 0    50   ~ 0
Data5
Text Label 3800 2200 0    50   ~ 0
Data6
Text Label 3800 2300 0    50   ~ 0
Data7
Text Notes 3950 2400 0    50   ~ 0
NOTE: Address and data pins have been switched around to facilitate easier routing.\nThis does not influence how the RAM works in any way.
Text GLabel 1450 1650 2    50   Input ~ 0
GND
Text GLabel 950  1650 0    50   Input ~ 0
VCC
Text Label 4200 2700 2    50   ~ 0
Data0
Text Label 4700 2700 0    50   ~ 0
Data1
Text Label 4200 2800 2    50   ~ 0
Data2
Text Label 4700 2800 0    50   ~ 0
Data3
Text Label 4200 2900 2    50   ~ 0
Data4
Text Label 4700 2900 0    50   ~ 0
Data5
Text Label 4200 3000 2    50   ~ 0
Data6
Text Label 4700 3000 0    50   ~ 0
Data7
Text GLabel 4700 2600 2    50   Input ~ 0
GND
Text GLabel 4200 2600 0    50   Input ~ 0
VCC
Text Label 950  2500 2    50   ~ 0
Addr_Low0
Text Label 1450 2500 0    50   ~ 0
Addr_Low1
Text Label 950  2600 2    50   ~ 0
Addr_Low2
Text Label 1450 2600 0    50   ~ 0
Addr_Low3
Text Label 950  2700 2    50   ~ 0
Addr_Low4
Text Label 1450 2700 0    50   ~ 0
Addr_Low5
Text Label 950  2800 2    50   ~ 0
Addr_Low6
Text Label 1450 2800 0    50   ~ 0
Addr_Low7
Text Label 1750 3950 0    50   ~ 0
CE
Text Label 1750 4150 0    50   ~ 0
IE
Text Label 1750 4050 0    50   ~ 0
OE
$Comp
L Device:C C1
U 1 1 5B2D92D7
P 4400 4900
F 0 "C1" H 4515 4946 50  0000 L CNN
F 1 "100n" H 4515 4855 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D10.0mm_W2.5mm_P5.00mm" H 4438 4750 50  0001 C CNN
F 3 "~" H 4400 4900 50  0001 C CNN
	1    4400 4900
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5B2D9360
P 4400 5050
F 0 "#PWR0105" H 4400 4800 50  0001 C CNN
F 1 "GND" H 4405 4877 50  0000 C CNN
F 2 "" H 4400 5050 50  0001 C CNN
F 3 "" H 4400 5050 50  0001 C CNN
	1    4400 5050
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0106
U 1 1 5B2D93B0
P 4400 4750
F 0 "#PWR0106" H 4400 4600 50  0001 C CNN
F 1 "VCC" H 4417 4923 50  0000 C CNN
F 2 "" H 4400 4750 50  0001 C CNN
F 3 "" H 4400 4750 50  0001 C CNN
	1    4400 4750
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J2
U 1 1 5B3623BC
P 1150 1850
F 0 "J2" H 1200 2267 50  0000 C CNN
F 1 "Address_High" H 1200 2176 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 1150 1850 50  0001 C CNN
F 3 "~" H 1150 1850 50  0001 C CNN
	1    1150 1850
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J1
U 1 1 5B3627C4
P 1150 2600
F 0 "J1" H 1200 3017 50  0000 C CNN
F 1 "Address_Low" H 1200 2926 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 1150 2600 50  0001 C CNN
F 3 "~" H 1150 2600 50  0001 C CNN
	1    1150 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J3
U 1 1 5B362B6D
P 4400 2800
F 0 "J3" H 4450 3217 50  0000 C CNN
F 1 "Data_IO" H 4450 3126 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 4400 2800 50  0001 C CNN
F 3 "~" H 4400 2800 50  0001 C CNN
	1    4400 2800
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J4
U 1 1 5B36317B
P 1550 4050
F 0 "J4" H 1656 4328 50  0000 C CNN
F 1 "Control" H 1656 4237 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 1550 4050 50  0001 C CNN
F 3 "~" H 1550 4050 50  0001 C CNN
	1    1550 4050
	1    0    0    -1  
$EndComp
$Comp
L Memory_EEPROM:28C256 U1
U 1 1 5B36442A
P 3400 2500
F 0 "U1" H 3200 3550 50  0000 C CNN
F 1 "28C256" H 3600 3550 50  0000 C CNN
F 2 "Package_DIP:DIP-28_W15.24mm_Socket" H 3400 2500 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc0006.pdf" H 3400 2500 50  0001 C CNN
	1    3400 2500
	1    0    0    -1  
$EndComp
NoConn ~ 1450 2050
$Comp
L 74xx:74LS04 U2
U 1 1 5B3E686C
P 5500 3900
F 0 "U2" H 5500 4217 50  0000 C CNN
F 1 "74LS04" H 5500 4126 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 5500 3900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 5500 3900 50  0001 C CNN
	1    5500 3900
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U2
U 2 1 5B3E68F8
P 5500 4100
F 0 "U2" H 5500 4417 50  0000 C CNN
F 1 "74LS04" H 5500 4326 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 5500 4100 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 5500 4100 50  0001 C CNN
	2    5500 4100
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U2
U 3 1 5B3E697D
P 5500 4250
F 0 "U2" H 5500 4567 50  0000 C CNN
F 1 "74LS04" H 5500 4476 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 5500 4250 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 5500 4250 50  0001 C CNN
	3    5500 4250
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U2
U 4 1 5B3E6A11
P 5500 4500
F 0 "U2" H 5500 4817 50  0000 C CNN
F 1 "74LS04" H 5500 4726 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 5500 4500 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 5500 4500 50  0001 C CNN
	4    5500 4500
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U2
U 5 1 5B3E6AB6
P 5500 4700
F 0 "U2" H 5500 5017 50  0000 C CNN
F 1 "74LS04" H 5500 4926 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 5500 4700 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 5500 4700 50  0001 C CNN
	5    5500 4700
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U2
U 6 1 5B3E6B72
P 5500 4900
F 0 "U2" H 5500 5217 50  0000 C CNN
F 1 "74LS04" H 5500 5126 50  0000 C CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 5500 4900 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 5500 4900 50  0001 C CNN
	6    5500 4900
	1    0    0    -1  
$EndComp
$Comp
L 74xx:74LS04 U2
U 7 1 5B3E6C21
P 6800 4300
F 0 "U2" H 7030 4346 50  0000 L CNN
F 1 "74LS04" H 7030 4255 50  0000 L CNN
F 2 "Package_DIP:DIP-14_W7.62mm" H 6800 4300 50  0001 C CNN
F 3 "http://www.ti.com/lit/gpn/sn74LS04" H 6800 4300 50  0001 C CNN
	7    6800 4300
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR03
U 1 1 5B3E6CA9
P 6800 3800
F 0 "#PWR03" H 6800 3650 50  0001 C CNN
F 1 "VCC" H 6817 3973 50  0000 C CNN
F 2 "" H 6800 3800 50  0001 C CNN
F 3 "" H 6800 3800 50  0001 C CNN
	1    6800 3800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5B3E6CF9
P 6800 4800
F 0 "#PWR04" H 6800 4550 50  0001 C CNN
F 1 "GND" H 6805 4627 50  0000 C CNN
F 2 "" H 6800 4800 50  0001 C CNN
F 3 "" H 6800 4800 50  0001 C CNN
	1    6800 4800
	1    0    0    -1  
$EndComp
Text Label 5800 4900 0    50   ~ 0
-CE
Text Label 5800 4700 0    50   ~ 0
-OE
Text Label 5800 4500 0    50   ~ 0
-IE
Text Label 5200 4500 2    50   ~ 0
IE
Text Label 5200 4700 2    50   ~ 0
OE
Text Label 5200 4900 2    50   ~ 0
CE
NoConn ~ 5200 4250
NoConn ~ 5200 4100
NoConn ~ 5200 3900
NoConn ~ 5800 4250
NoConn ~ 5800 4100
NoConn ~ 5800 3900
$EndSCHEMATC
