EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J1
U 1 1 5B438CDA
P 1500 1500
F 0 "J1" H 1550 1917 50  0000 C CNN
F 1 "Port_West" H 1550 1826 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x05_P2.54mm_Vertical" H 1500 1500 50  0001 C CNN
F 3 "~" H 1500 1500 50  0001 C CNN
	1    1500 1500
	1    0    0    -1  
$EndComp
Text GLabel 1300 1300 0    50   Input ~ 0
VCC
Text GLabel 1800 1300 2    50   Input ~ 0
GND
Text Label 1300 1400 2    50   ~ 0
Data0
Text Label 1800 1400 0    50   ~ 0
Data1
Text Label 1300 1500 2    50   ~ 0
Data2
Text Label 1800 1500 0    50   ~ 0
Data3
Text Label 1300 1600 2    50   ~ 0
Data4
Text Label 1800 1600 0    50   ~ 0
Data5
Text Label 1300 1700 2    50   ~ 0
Data6
Text Label 1800 1700 0    50   ~ 0
Data7
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J2
U 1 1 5B438EFD
P 1500 2350
F 0 "J2" H 1550 2767 50  0000 C CNN
F 1 "Port_East" H 1550 2676 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x05_P2.54mm_Vertical" H 1500 2350 50  0001 C CNN
F 3 "~" H 1500 2350 50  0001 C CNN
	1    1500 2350
	1    0    0    -1  
$EndComp
Text GLabel 1300 2150 0    50   Input ~ 0
VCC
Text GLabel 1800 2150 2    50   Input ~ 0
GND
Text Label 1300 2250 2    50   ~ 0
Data0
Text Label 1800 2250 0    50   ~ 0
Data1
Text Label 1300 2350 2    50   ~ 0
Data2
Text Label 1800 2350 0    50   ~ 0
Data3
Text Label 1300 2450 2    50   ~ 0
Data4
Text Label 1800 2450 0    50   ~ 0
Data5
Text Label 1300 2550 2    50   ~ 0
Data6
Text Label 1800 2550 0    50   ~ 0
Data7
$EndSCHEMATC
