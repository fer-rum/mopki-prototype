EESchema Schematic File Version 4
LIBS:mopki_switch_input-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L power:GND #PWR0101
U 1 1 5B1951F8
P 11100 6300
F 0 "#PWR0101" H 11100 6050 50  0001 C CNN
F 1 "GND" H 11105 6127 50  0000 C CNN
F 2 "" H 11100 6300 50  0001 C CNN
F 3 "" H 11100 6300 50  0001 C CNN
	1    11100 6300
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0102
U 1 1 5B1952DF
P 10750 6300
F 0 "#PWR0102" H 10750 6150 50  0001 C CNN
F 1 "VCC" H 10767 6473 50  0000 C CNN
F 2 "" H 10750 6300 50  0001 C CNN
F 3 "" H 10750 6300 50  0001 C CNN
	1    10750 6300
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5B19534E
P 11100 6300
F 0 "#FLG0101" H 11100 6375 50  0001 C CNN
F 1 "PWR_FLAG" H 11100 6474 50  0000 C CNN
F 2 "" H 11100 6300 50  0001 C CNN
F 3 "~" H 11100 6300 50  0001 C CNN
	1    11100 6300
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 5B195370
P 10750 6300
F 0 "#FLG0102" H 10750 6375 50  0001 C CNN
F 1 "PWR_FLAG" H 10750 6473 50  0000 C CNN
F 2 "" H 10750 6300 50  0001 C CNN
F 3 "~" H 10750 6300 50  0001 C CNN
	1    10750 6300
	-1   0    0    1   
$EndComp
Text Label 1150 1150 2    50   ~ 0
DataOut0
Text Label 1650 1150 0    50   ~ 0
DataOut1
Text Label 1150 1250 2    50   ~ 0
DataOut2
Text Label 1650 1250 0    50   ~ 0
DataOut3
Text Label 1150 1350 2    50   ~ 0
DataOut4
Text Label 1650 1350 0    50   ~ 0
DataOut5
Text Label 1150 1450 2    50   ~ 0
DataOut6
Text Label 1650 1450 0    50   ~ 0
DataOut7
Text GLabel 1650 1050 2    50   Input ~ 0
GND
Text GLabel 1150 1050 0    50   Input ~ 0
VCC
$Comp
L Switch:SW_DIP_x08 SW1
U 1 1 5B1A9105
P 3300 1350
F 0 "SW1" H 3300 2017 50  0000 C CNN
F 1 "SW_DIP_x08" H 3300 1926 50  0000 C CNN
F 2 "Button_Switch_THT:SW_DIP_SPSTx08_Slide_9.78x22.5mm_W7.62mm_P2.54mm" H 3300 1350 50  0001 C CNN
F 3 "" H 3300 1350 50  0001 C CNN
	1    3300 1350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 950  3000 1050
Wire Wire Line
	3000 1150 3000 1050
Connection ~ 3000 1050
Wire Wire Line
	3000 1150 3000 1250
Connection ~ 3000 1150
Wire Wire Line
	3000 1250 3000 1350
Connection ~ 3000 1250
Wire Wire Line
	3000 1450 3000 1550
Wire Wire Line
	3000 1550 3000 1650
Connection ~ 3000 1550
Wire Wire Line
	3000 1450 3000 1350
Connection ~ 3000 1450
Connection ~ 3000 1350
Wire Wire Line
	3000 950  3000 850 
Connection ~ 3000 950 
$Comp
L power:VCC #PWR0103
U 1 1 5B1A9AEB
P 3000 850
F 0 "#PWR0103" H 3000 700 50  0001 C CNN
F 1 "VCC" H 3017 1023 50  0000 C CNN
F 2 "" H 3000 850 50  0001 C CNN
F 3 "" H 3000 850 50  0001 C CNN
	1    3000 850 
	1    0    0    -1  
$EndComp
Text Label 3600 950  0    50   ~ 0
DataOut0
Text Label 3600 1050 0    50   ~ 0
DataOut1
Text Label 3600 1150 0    50   ~ 0
DataOut2
Text Label 3600 1250 0    50   ~ 0
DataOut3
Text Label 3600 1350 0    50   ~ 0
DataOut4
Text Label 3600 1450 0    50   ~ 0
DataOut5
Text Label 3600 1550 0    50   ~ 0
DataOut6
Text Label 3600 1650 0    50   ~ 0
DataOut7
$Comp
L Device:R R1
U 1 1 5B1A9B5F
P 3250 2150
F 0 "R1" V 3200 2000 50  0000 C CNN
F 1 "10K" V 3250 2150 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3180 2150 50  0001 C CNN
F 3 "~" H 3250 2150 50  0001 C CNN
	1    3250 2150
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 5B1A9BF1
P 3250 2250
F 0 "R2" V 3200 2100 50  0000 C CNN
F 1 "10K" V 3250 2250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3180 2250 50  0001 C CNN
F 3 "~" H 3250 2250 50  0001 C CNN
	1    3250 2250
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5B1A9CCC
P 3250 2350
F 0 "R3" V 3200 2200 50  0000 C CNN
F 1 "10K" V 3250 2350 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3180 2350 50  0001 C CNN
F 3 "~" H 3250 2350 50  0001 C CNN
	1    3250 2350
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 5B1A9CD3
P 3250 2450
F 0 "R4" V 3200 2300 50  0000 C CNN
F 1 "10K" V 3250 2450 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3180 2450 50  0001 C CNN
F 3 "~" H 3250 2450 50  0001 C CNN
	1    3250 2450
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 5B1A9E40
P 3250 2550
F 0 "R5" V 3200 2400 50  0000 C CNN
F 1 "10K" V 3250 2550 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3180 2550 50  0001 C CNN
F 3 "~" H 3250 2550 50  0001 C CNN
	1    3250 2550
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 5B1A9E47
P 3250 2650
F 0 "R6" V 3200 2500 50  0000 C CNN
F 1 "10K" V 3250 2650 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3180 2650 50  0001 C CNN
F 3 "~" H 3250 2650 50  0001 C CNN
	1    3250 2650
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 5B1A9E4E
P 3250 2750
F 0 "R7" V 3200 2600 50  0000 C CNN
F 1 "10K" V 3250 2750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3180 2750 50  0001 C CNN
F 3 "~" H 3250 2750 50  0001 C CNN
	1    3250 2750
	0    1    1    0   
$EndComp
$Comp
L Device:R R8
U 1 1 5B1A9E55
P 3250 2850
F 0 "R8" V 3200 2700 50  0000 C CNN
F 1 "10K" V 3250 2850 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 3180 2850 50  0001 C CNN
F 3 "~" H 3250 2850 50  0001 C CNN
	1    3250 2850
	0    1    1    0   
$EndComp
Wire Wire Line
	3100 2150 3000 2150
Wire Wire Line
	3100 2250 3000 2250
Wire Wire Line
	3000 2150 3000 2250
Wire Wire Line
	3000 2250 3000 2350
Wire Wire Line
	3000 2350 3100 2350
Connection ~ 3000 2250
Wire Wire Line
	3000 2350 3000 2450
Wire Wire Line
	3000 2450 3100 2450
Connection ~ 3000 2350
Wire Wire Line
	3000 2450 3000 2550
Wire Wire Line
	3000 2550 3100 2550
Connection ~ 3000 2450
Wire Wire Line
	3000 2550 3000 2650
Wire Wire Line
	3000 2650 3100 2650
Connection ~ 3000 2550
Wire Wire Line
	3000 2650 3000 2750
Wire Wire Line
	3000 2750 3100 2750
Connection ~ 3000 2650
Wire Wire Line
	3000 2750 3000 2850
Wire Wire Line
	3000 2850 3100 2850
Connection ~ 3000 2750
$Comp
L power:GND #PWR0104
U 1 1 5B1AA645
P 3000 2850
F 0 "#PWR0104" H 3000 2600 50  0001 C CNN
F 1 "GND" H 3005 2677 50  0000 C CNN
F 2 "" H 3000 2850 50  0001 C CNN
F 3 "" H 3000 2850 50  0001 C CNN
	1    3000 2850
	1    0    0    -1  
$EndComp
Connection ~ 3000 2850
Text Label 3400 2150 0    50   ~ 0
DataOut0
Text Label 3400 2250 0    50   ~ 0
DataOut1
Text Label 3400 2350 0    50   ~ 0
DataOut2
Text Label 3400 2450 0    50   ~ 0
DataOut3
Text Label 3400 2550 0    50   ~ 0
DataOut4
Text Label 3400 2650 0    50   ~ 0
DataOut5
Text Label 3400 2750 0    50   ~ 0
DataOut6
Text Label 3400 2850 0    50   ~ 0
DataOut7
Text GLabel 10750 6300 0    50   Input ~ 0
VCC
Text GLabel 11100 6300 0    50   Input ~ 0
GND
$Comp
L Connector_Generic:Conn_02x05_Odd_Even J1
U 1 1 5B3B4B59
P 1350 1250
F 0 "J1" H 1400 1667 50  0000 C CNN
F 1 "Data_Out" H 1400 1576 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x05_P2.54mm_Vertical" H 1350 1250 50  0001 C CNN
F 3 "~" H 1350 1250 50  0001 C CNN
	1    1350 1250
	1    0    0    -1  
$EndComp
$EndSCHEMATC
