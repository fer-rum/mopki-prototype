# MoPKi Prototype

The first prototype for the __Modular Processor Kit__.

This repository contains the first draft of the MoPKi project.
It is kept as reference and will likely not be updated.

__Beware of bugs!__ Some design issues of the presented designs are known, others still lurk in the shadows.

Use the designs at your own risk.

## Tools
The simulator code is written using _Python 3.5_.

The PCB designs were made with _KiCad 5.0.0-rc3_.