from structure.design import Design
from components.modules import *

# Set up the architecture
d = Design()
psu = PSU("PSU")
bus0 = Bus("Bus0")
bus1 = Bus("Bus1")
input_switch = SwitchInput8bit("SwIn")
clock = Clock("Clock")
reg0 = Register8bit("Reg0")
reg1 = Register8bit("Reg1")

d.add_module(psu)
d.add_module(bus0)
d.add_module(bus1)
d.add_module(input_switch)
d.add_module(clock)
d.add_module(reg0)
d.add_module(reg1)

d.link_connectors(psu.conn_power_out, bus0.conn_power_in)
d.link_connectors(psu.conn_power_out, bus1.conn_power_in)
d.link_connectors(psu.conn_power_out, clock.conn_power_in)
d.link_connectors("Bus0:North", "Bus1:South")
d.link_connectors("Bus0:IO_0", "Reg0:Input")
d.link_connectors("Bus1:IO_0", "Reg1:Input")
d.link_connectors("Clock:ClockOut", "Reg0:Clock")
d.link_connectors("Clock:ClockOut", "Reg1:Clock")

d.link_pins("PSU:PowerOut:PWR", "SwIn:Output:PWR")
d.link_pins("PSU:PowerOut:GND", "SwIn:Output:GND")

d.link_pins("SwIn:Output:D0", "Reg0:InputEnable:D0")
d.link_pins("SwIn:Output:D1", "Reg1:InputEnable:D0")
# And simulate

d.simulate_time_step()
input_switch.toggle("D0")
d.simulate_time_step()
