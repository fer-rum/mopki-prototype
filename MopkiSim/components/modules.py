from typing import Dict, Set

from structure.modules import Module
from components.connectors import Conn10pinPowered, Conn2pinPowered, Conn1pinSignal
from structure.pins import SignalLevel, PinType, pin_names


class PSU(Module):

    def __init__(self, name: str):

        super(PSU, self).__init__(name)
        self.conn_power_out = self.add_connector("PowerOut", Conn2pinPowered)

        self.powered = True
        self.grounded = True

    def propagate_power(self):
        self.conn_power_out.power()
        self.conn_power_out.ground()

    def get_input(self):
        pass

    def evaluate(self):
        pass

    def set_output(self):
        pass

    def clear_state(self):
        pass


class Negation(Module):
    
    def __init__(self, name: str):

        super(Negation, self).__init__(name)
        self.add_connector("Input", Conn10pinPowered)
        self.add_connector("Output", Conn10pinPowered)

        self._input: Conn10pinPowered = self.connectors[0]
        self._output: Conn10pinPowered = self.connectors[1]

        self._state: Dict[str, SignalLevel] = {}

    def get_input(self):
        self._state = self._input.get_state()

    def evaluate(self):
        for name, level in self._state.items():
            if level == SignalLevel.HIGH:
                self._state[name] = SignalLevel.LOW
            elif level == SignalLevel.LOW:
                self._state[name] = SignalLevel.HIGH

    def set_output(self):
        self._output.set_state(self._state)

    def clear_state(self):
        self._state.clear()


class Bus(Module):

    io0_name: str = "IO_0"
    io1_name: str = "IO_1"
    io2_name: str = "IO_2"
    io3_name: str = "IO_3"

    def __init__(self, name: str):

        super(Bus, self).__init__(name)

        self.add_connector(Bus.io0_name, Conn10pinPowered)
        self.add_connector(Bus.io1_name, Conn10pinPowered)
        self.add_connector(Bus.io2_name, Conn10pinPowered)
        self.add_connector(Bus.io3_name, Conn10pinPowered)
        self.add_connector("North", Conn10pinPowered)
        self.add_connector("South", Conn10pinPowered)
        self.conn_power_in = self.add_connector("PowerIn", Conn2pinPowered)

        self._state: Dict[str, SignalLevel] = {}

        self._data_connectors: Set[Conn10pinPowered] = set(c for c in self.connectors if isinstance(c, Conn10pinPowered))

    def get_input(self):

        for connector in self._data_connectors:

            for (key, value) in connector.get_state().items():
                if value == SignalLevel.UNDEFINED:
                    continue

                if key not in self._state or self._state[key] == SignalLevel.UNDEFINED:
                    self._state[key] = value
                else:
                    assert self._state[key] == value, "Conflicting drivers on Bus " + self.local_name

    def evaluate(self):
        # nothing to do here
        pass

    def set_output(self):

        for connector in self._data_connectors:
            connector.set_state(self._state)

    def clear_state(self):
        self._state.clear()


class SwitchInput8bit(Module):

    def __init__(self, name: str):

        super(SwitchInput8bit, self).__init__(name)
        self.conn_output: Conn10pinPowered = self.add_connector("Output", Conn10pinPowered)

        self._state: Dict[str, SignalLevel] = {i: SignalLevel.LOW for i in self.connectors[0].get_state().keys()}

    def toggle(self, switch_name: str):
        assert switch_name in self._state

        current_level = self._state[switch_name]

        assert (current_level == SignalLevel.HIGH) or (current_level == SignalLevel.LOW)

        if current_level == SignalLevel.HIGH:
            self._state[switch_name] = SignalLevel.LOW
        elif current_level == SignalLevel.LOW:
            self._state[switch_name] = SignalLevel.HIGH

    def enable(self, switch_name: str):
        self._state[switch_name] = SignalLevel.HIGH

    def disable(self, switch_name: str):
        self._state[switch_name] = SignalLevel.LOW

    def set_state(self, new_state: Dict[str, SignalLevel]):
        if not new_state:
            raise ValueError("State must not be None")

        for (name, value) in new_state.items():
            if name in self._state:
                self._state[name] = value
            else:
                raise ValueError("Attempt to set non-existent state " + name)

    def get_input(self):
        pass

    def evaluate(self):
        pass

    def set_output(self):
        self.connectors[0].set_state(self._state)

    def clear_state(self):
        pass  # state gets retained intentionally


class Register8bit(Module):
    """

    Input is clock-sensitive and gets executed on rising edge when input enable is set.
    Output is not clock sensitive and gets executed when output enable is set.
    If output enable is not set, the outputs are disconnected.
    """

    def __init__(self, name: str):
        super(Register8bit, self).__init__(name)

        self._input: Conn10pinPowered = self.add_connector("Input", Conn10pinPowered)
        self._output: Conn10pinPowered = self.add_connector("Output", Conn10pinPowered)
        self._reset: Conn1pinSignal = self.add_connector("Reset", Conn1pinSignal)
        self._ie: Conn1pinSignal = self.add_connector("InputEnable", Conn1pinSignal)
        self._oe: Conn1pinSignal = self.add_connector("OutputEnable", Conn1pinSignal)
        self._clock: Conn1pinSignal = self.add_connector("Clock", Conn1pinSignal)

        self._state: Dict[str, SignalLevel] = {i: SignalLevel.LOW for i in self._input.pin_names(PinType.SIGNAL)}
        self._last_clock: SignalLevel = SignalLevel.UNDEFINED

    def set_output(self):
        if self._oe.has_level(SignalLevel.HIGH):
            self._output.set_state(self._state)
        else:
            self._output.set_state({i: SignalLevel.DISCONNECTED for i in self._input.pin_names(PinType.SIGNAL)})

    def get_input(self):

        #  Check reset
        if self._reset.has_level(SignalLevel.HIGH):
            self._state: Dict[str, SignalLevel] = {i: SignalLevel.LOW for i in self._input.pin_names(PinType.SIGNAL)}
            self._last_clock: SignalLevel = SignalLevel.UNDEFINED

        # Check clock
        elif self._last_clock == SignalLevel.LOW and self._clock.has_level(SignalLevel.HIGH):
            # We have a rising edge
            if self._ie.has_level(SignalLevel.HIGH):
                self._state = self._input.get_state()

        self._last_clock = self._clock.get_level()

    def clear_state(self):
        """
        The state of the register is not cleared between time steps since it is a memory device.
        :return:
        """
        pass

    def evaluate(self):
        pass


class Clock(Module):

    clock_out_name: str = "ClockOut"
    power_in_name: str = "PowerIn"

    def __init__(self, name: str):
        super(Clock, self).__init__(name)

        self.conn_power_in = self.add_connector(Clock.power_in_name, Conn2pinPowered)
        self.conn_clock_out = self.add_connector(Clock.clock_out_name, Conn1pinSignal)
        self._clock_state = False

    def set_output(self):

        if self._clock_state:
            return {Clock.clock_out_name: SignalLevel.HIGH}
        else:
            return {Clock.clock_out_name: SignalLevel.LOW}

    def get_input(self):
        """
        This module has no input pins
        :return:
        """
        pass

    def evaluate(self):
        self._clock_state = not self._clock_state

    def clear_state(self):
        self._clock_state = False


class Transceiver(Module):

    state_nc: Dict[str, SignalLevel] = {name: SignalLevel.DISCONNECTED
                                        for name in pin_names(PinType.SIGNAL, 0, 7)}

    def __init__(self, name: str):
        super(Transceiver, self).__init__(name)

        self.conn_side_a: Conn10pinPowered = self.add_connector("SideA", Conn10pinPowered)
        self.conn_side_b: Conn10pinPowered = self.add_connector("SideB", Conn10pinPowered)
        self.conn_chip_enable: Conn1pinSignal = self.add_connector("ChipEnable", Conn1pinSignal)
        self.conn_direction: Conn1pinSignal = self.add_connector("Direction", Conn1pinSignal)

        # Intermediate storage of direction state
        # If no definite statement is made, they fall back to None
        self._a_to_b: bool = None
        self._enabled: bool = None
        self._state_a: Dict[str, SignalLevel] = None
        self._state_b: Dict[str, SignalLevel] = None

    def get_input(self):
        self._state_a = self.conn_side_a.get_state()
        self._state_b = self.conn_side_b.get_state()

        if self.conn_chip_enable.has_level(SignalLevel.HIGH):
            self._enabled = True
        elif self.conn_chip_enable.has_level(SignalLevel.LOW):
            self._enabled = False
        else:
            self._enabled = None

        if self.conn_direction.has_level(SignalLevel.HIGH):
            self._a_to_b = True
        elif self.conn_direction.has_level(SignalLevel.LOW):
            self._a_to_b = False
        else:
            self._a_to_b = None
            print("\t" + self.global_name + ": Direction pin is not driven")

    def evaluate(self):
        pass

    def set_output(self):

        if self._enabled is None:
            return

        if self._a_to_b is None:
            return

        if self._enabled:
            if self._a_to_b:
                self.conn_side_b.set_state(self._state_a)
            else:
                self.conn_side_a.set_state(self._state_b)
        else:
            self.conn_side_a.disconnect()
            self.conn_side_b.disconnect()

    def clear_state(self):
        self._a_to_b = None
        self._enabled = None
        self._state_a: Dict[str, SignalLevel] = None
        self._state_b: Dict[str, SignalLevel] = None


class Adder(Module):

    class AdditionResult(object):

        def __init__(self, the_sum: SignalLevel, carry: SignalLevel):
            self.sum = the_sum
            self.carry = carry

    overflow_out_name: str = "Overflow"
    zero_out_name: str = "Zero"
    sign_out_name: str = "Sign"
    carry_out_name: str = "CarryOut"
    carry_in_name: str = "CarryIn"

    arg_0_name: str = "Arg0"
    arg_1_name: str = "Arg1"
    result_name: str = "Result"

    def __init__(self, name):

        super(Adder, self).__init__(name)

        # inputs
        self.conn_arg0: Conn10pinPowered = self.add_connector(Adder.arg_0_name, Conn10pinPowered)
        self.conn_arg1: Conn10pinPowered = self.add_connector(Adder.arg_1_name, Conn10pinPowered)
        self.conn_carry_in: Conn1pinSignal = self.add_connector(Adder.carry_in_name, Conn1pinSignal)

        # outputs
        self.conn_result: Conn10pinPowered = self.add_connector(Adder.result_name, Conn10pinPowered)
        self.conn_carry_out: Conn1pinSignal = self.add_connector(Adder.carry_out_name, Conn1pinSignal)
        self.conn_zero: Conn1pinSignal = self.add_connector(Adder.zero_out_name, Conn1pinSignal)
        self.conn_sign: Conn1pinSignal = self.add_connector(Adder.sign_out_name, Conn1pinSignal)
        self.conn_overflow: Conn1pinSignal = self.add_connector(Adder.overflow_out_name, Conn1pinSignal)

        # internal
        self._arg0: Dict[str, SignalLevel] = {}
        self._arg1: Dict[str, SignalLevel] = {}
        self._result: Dict[str, SignalLevel] = {}
        self._carry_in: SignalLevel = SignalLevel.UNDEFINED
        self._carry_out: SignalLevel = SignalLevel.UNDEFINED
        self._sign: SignalLevel = SignalLevel.UNDEFINED
        self._zero: SignalLevel = SignalLevel.UNDEFINED
        self._overflow: SignalLevel = SignalLevel.UNDEFINED

    @staticmethod
    def add_bits(arg0: SignalLevel, arg1: SignalLevel, arg2: SignalLevel):

        the_sum: int = 0
        for arg in (arg0, arg1, arg2):
            # if any of the arguments is undefined, the result is undefined
            if arg not in (SignalLevel.LOW, SignalLevel.HIGH):
                return Adder.AdditionResult(SignalLevel.UNDEFINED, SignalLevel.UNDEFINED)
            elif arg == SignalLevel.HIGH:
                the_sum += 1

        assert the_sum <= 3, "The sum of three bits should not exceed 3"

        result_lookup = {0: Adder.AdditionResult(SignalLevel.LOW, SignalLevel.LOW),
                         1: Adder.AdditionResult(SignalLevel.HIGH, SignalLevel.LOW),
                         2: Adder.AdditionResult(SignalLevel.LOW, SignalLevel.HIGH),
                         3: Adder.AdditionResult(SignalLevel.HIGH, SignalLevel.HIGH)}

        return result_lookup[the_sum]

    def get_input(self):
        self._arg0 = self.conn_arg0.get_state()
        self._arg1 = self.conn_arg1.get_state()
        self._carry_in = self.conn_carry_in.get_state()["D0"]

    def evaluate(self):

        carry_bit = self._carry_in
        self._zero = SignalLevel.HIGH

        for bit_name in pin_names(PinType.SIGNAL, 0, 7):
            result: Adder.AdditionResult = Adder.add_bits(self._arg0[bit_name], self._arg1[bit_name], carry_bit)
            self._result[bit_name] = result.sum
            carry_bit = result.carry

            if result.sum == SignalLevel.HIGH:
                self._zero = SignalLevel.LOW
            elif result.sum in (SignalLevel.UNDEFINED, SignalLevel.DISCONNECTED):
                self._zero = SignalLevel.UNDEFINED

        self._carry_out = carry_bit
        self._sign = self._result["D7"]

        if SignalLevel.UNDEFINED in (self._arg0["D7"], self._arg1["D7"], self._result["D7"]):
            self._overflow = SignalLevel.UNDEFINED
        elif self._arg0["D7"] == self._arg1["D7"] and self._arg0["D7"] == self._result["D7"]:
            self._overflow = SignalLevel.LOW
        else:
            self._overflow = SignalLevel.HIGH

    def set_output(self):
        self.conn_result.set_state(self._result)
        self.conn_carry_out.set_state({"D0": self._carry_out})
        self.conn_sign.set_state({"D0": self._sign})
        self.conn_zero.set_state({"D0": self._zero})
        self.conn_overflow.set_state({"D0": self._overflow})

    def clear_state(self):
        self._arg0.clear()
        self._arg1.clear()
        self._result.clear()
        self._carry_in = SignalLevel.UNDEFINED
        self._carry_out = SignalLevel.UNDEFINED
        self._sign: SignalLevel = SignalLevel.UNDEFINED
        self._zero: SignalLevel = SignalLevel.UNDEFINED
        self._overflow: SignalLevel = SignalLevel.UNDEFINED
