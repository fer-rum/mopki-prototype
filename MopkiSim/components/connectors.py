from typing import List, Tuple

from structure.connectors import Connector
from structure.modules import Module
from structure.pins import PinType, SignalLevel


class Conn1pinSignal(Connector):
    """
    Connector with one signal pin.
    The pin name is the same as the connector name.
    Not powered, not grounded (obviously).
    """

    def __init__(self,
                 name: str,
                 in_module: Module):

        super(Conn1pinSignal, self).__init__(name, in_module)
        self.pin_d0 = self.add_pin(0, "D0", PinType.SIGNAL)

    def has_level(self, assumed: SignalLevel) -> bool:
        return self.pin_assignment[0].signal_level == assumed

    def get_level(self) -> SignalLevel:
        return self.pin_assignment[0].signal_level


class Conn2pinPowered(Connector):

    def __init__(self,
                 name: str,
                 in_module: Module):
        super(Conn2pinPowered, self).__init__(name, in_module)
        self.pin_pwr = self.add_pin(0, "PWR", PinType.POWER)
        self.pin_gnd = self.add_pin(1, "GND", PinType.GROUND)


class Conn10pinPowered(Connector):
    """
    Connector with power, ground and 8 signal pins.
    Pin 0 is Power, Pin 1 is Ground, Pin 2 ... 9 is Data 0 ... 7
    """

    def __init__(self,
                 name: str,
                 in_module: Module):
        super(Conn10pinPowered, self).__init__(name, in_module)
        self.pin_pwr = self.add_pin(0, "PWR", PinType.POWER)
        self.pin_gnd = self.add_pin(1, "GND", PinType.GROUND)
        self.pin_d0 = self.add_pin(2, "D0", PinType.SIGNAL)
        self.pin_d1 = self.add_pin(3, "D1", PinType.SIGNAL)
        self.pin_d2 = self.add_pin(4, "D2", PinType.SIGNAL)
        self.pin_d3 = self.add_pin(5, "D3", PinType.SIGNAL)
        self.pin_d4 = self.add_pin(6, "D4", PinType.SIGNAL)
        self.pin_d5 = self.add_pin(7, "D5", PinType.SIGNAL)
        self.pin_d6 = self.add_pin(8, "D6", PinType.SIGNAL)
        self.pin_d7 = self.add_pin(9, "D7", PinType.SIGNAL)

