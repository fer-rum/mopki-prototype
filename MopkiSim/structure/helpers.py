class HierarchicallyNamed(object):
    pass


class HierarchicallyNamed(object):

    SEPARATOR = ':'

    def __init__(self, local_name: str, parent: HierarchicallyNamed = None):

        assert HierarchicallyNamed.SEPARATOR not in local_name,\
            "Hierarchical names may not contain \"" + HierarchicallyNamed.SEPARATOR + "\""

        self.parent: HierarchicallyNamed = parent
        self.local_name: str = local_name

        self.hierarchy_level: int = 0
        if parent:
            self.hierarchy_level = parent.hierarchy_level + 1

        if not self.parent:
            self.global_name = self.local_name
        else:
            self.global_name = self.parent.global_name + HierarchicallyNamed.SEPARATOR + self.local_name
