
from abc import abstractmethod
from typing import List
from structure.connectors import Connector
from structure.helpers import HierarchicallyNamed


class Module(HierarchicallyNamed, object):

    def __init__(self, name: str):
        assert name, "Modules must be named"

        super(Module, self).__init__(name)
        self.connectors: List[Connector] = []
        self.powered: bool = False
        self.grounded: bool = False

    def add_connector(self, connector_name, connector_type) -> Connector:
        new_connector = connector_type(connector_name, self)
        self.connectors.append(new_connector)
        return new_connector

    def propagate_power(self):
        """
        Evaluate whether the module is powered and continue distributing the power (and ground)
        :return:
        """
        for connector in self.connectors:
            if connector.is_powered():
                self.powered = True
                break

        for connector in self.connectors:
            if connector.is_grounded():
                self.grounded = True
                break

        if not self.powered:
            print("\t\t\tModule " + self.global_name + " is not powered")
        if not self.grounded:
            print("\t\t\tModule " + self.global_name + " is not grounded")

        if self.powered and self.grounded:
            for connector in self.connectors:
                connector.power()
                connector.ground()

    def clear_connectors(self):
        """
        Resets all connector signal states for the next iteration
        :return:
        """
        for connector in self.connectors:
            connector.clear()

    @abstractmethod
    def get_input(self):
        """
        Read all supposed Input pins of the connectors.
        After this call the values of the pins may be cleared
        :return:
        """
        pass

    @abstractmethod
    def evaluate(self):
        """
        Calculate the new internal state form the input gotten earlier.
        :return:
        """
        pass

    @abstractmethod
    def set_output(self):
        """
        Translate the current internal state to the Output pins
        :return:
        """
        pass

    @abstractmethod
    def clear_state(self):
        pass

    def update(self):
        self.propagate_power()

        # without power nothing works
        if not (self.powered and self.grounded):
            print("\t\t\tAborted evaluating " + self.global_name)
            return

        self.get_input()
        # self.clear_connectors()
        self.evaluate()
        self.set_output()

    def resolve_connector(self, connector_name: str):
        return next((c for c in self.connectors if c.local_name == connector_name), None)

    def __str__(self):
        return "Module " + self.local_name + "(" + self.__class__.__name__ + ")"
