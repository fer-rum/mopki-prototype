from typing import Dict, Set, List, Tuple
from structure.pins import Pin, PinType, PowerPin, GroundPin, SignalPin, SignalLevel
from structure.helpers import HierarchicallyNamed


class Connector(HierarchicallyNamed, object):

    def __init__(self,
                 connector_name: str,
                 in_module: HierarchicallyNamed,
                 pin_layout: List[Tuple[int, str, PinType]] = None):
        from structure.modules import Module

        assert isinstance(in_module, Module)

        assert in_module, "Connector can not be created without being in a module"

        super(Connector, self).__init__(connector_name, in_module)

        self.pin_assignment: Dict[int, Pin] = {}
        self._signal_pins: Set[SignalPin] = set()
        self._power_pins: Set[PowerPin] = set()
        self._ground_pins: Set[GroundPin] = set()

        if pin_layout:
            for (index, name, pin_type) in pin_layout:
                self.add_pin(index, name, pin_type)

    def add_pin(self,
                index: int,
                name: str,
                pin_type: PinType):

        assert index >= 0, \
            "Smallest acceptable pin index in connector is 0"
        assert index not in self.pin_assignment, \
            "Pin index " + str(index) + " is already taken"
        for pin in self.pin_assignment.values():
            assert name != pin.local_name, \
                "Pin name already in use in this connector"

        if pin_type == PinType.POWER:
            new_pin = PowerPin(name, self)
            self.pin_assignment[index] = new_pin
            self._power_pins.add(new_pin)
            return new_pin

        elif pin_type == PinType.GROUND:
            new_pin = GroundPin(name, self)
            self.pin_assignment[index] = new_pin
            self._ground_pins.add(new_pin)
            return new_pin

        elif pin_type == PinType.SIGNAL:
            new_pin = SignalPin(name, self)
            self.pin_assignment[index] = new_pin
            self._signal_pins.add(new_pin)
            return new_pin

        else:
            raise ValueError("Invalid pin type passed to connector")

    def is_powered(self) -> bool:
        """
        Check whether the connector is powered by one of its pins
        :return: True if the connector has a power pin that is HIGH, false otherwise
        """
        for pin in self._power_pins:
            if pin.signal_level == SignalLevel.HIGH:
                return True
        return False

    def power(self):
        """
        Mark the connector as powered and drive all power pins
        :return: Nothing
        """
        for pin in self._power_pins:
            pin.drive(SignalLevel.HIGH)

    def is_grounded(self) -> bool:
        """
        Check whether the connector is grounded by one of its pins
        :return: True if the connector has a ground pin that is LOW, false otherwise
        """
        for pin in self._ground_pins:
            if pin.signal_level == SignalLevel.LOW:
                return True
        return False

    def ground(self):
        """
        Mark the connector as grounded and drive all ground pins
        :return: Nothing
        """
        for pin in self._ground_pins:
            pin.drive(SignalLevel.LOW)

    def get_state(self) -> Dict[str, SignalLevel]:
        """
        Gets an association from the local pin names to their signal level
        :return:
        """
        return {pin.local_name: pin.signal_level for pin in self._signal_pins}

    def set_state(self, new_state: Dict[str, SignalLevel]):
        for pin in self._signal_pins:
            if pin.local_name in new_state:
                pin.drive(new_state[pin.local_name])

    def clear(self):
        """
        Sets all signal pins to the undefined state, so they can be driven anew
        :return:
        """
        for pin in self._signal_pins:
            pin.reset()

    def disconnect(self):

        for pin in self._signal_pins:
            pin.disconnect()

    def resolve_pin(self, pin_name: str) -> Pin:
        return next((p for p in self.pin_assignment.values() if p.local_name == pin_name), None)

    def pin_names(self, pin_type: PinType):

        if pin_type == PinType.POWER:
            return list(pin.local_name for pin in self._power_pins)
        elif pin_type == PinType.GROUND:
            return list(pin.local_name for pin in self._ground_pins)
        elif pin_type == PinType.SIGNAL:
            return list(pin.local_name for pin in self._signal_pins)
        else:
            return None
