
from typing import List, Set, Dict

from structure.connectors import Connector
from structure.modules import Module
from structure.helpers import HierarchicallyNamed
from structure.net import Net
from structure.pins import Pin


class Design(object):

    def __init__(self):
        self.modules: Set[Module] = set()
        self._nets: Set[Net] = set()
        self._delta_list: Dict[int, Set[Module]] = {}
        self.current_delta: int = 0

    def add_module(self, new_module: Module):
        """
        Adds a new module to the design. If the module is None an assertion will trigger.
        :param new_module: A subclass instance of structure.modules.Module. Must not be None
        :return: The newly added module
        """
        assert new_module
        self.modules.add(new_module)
        return new_module

    def schedule_module_next_delta(self, to_schedule: Module):

        next_delta = self.current_delta + 1

        if next_delta not in self._delta_list.keys():
            self._delta_list[next_delta] = set()

        self._delta_list[next_delta].add(to_schedule)

    def resolve_module(self, name: str) -> Module:
        """

        :param name: Can either be the a global or the local name of the module
        :return: The module given by the name or None if it doesn't exist
        """
        module_name = name.split(HierarchicallyNamed.SEPARATOR)[0]
        assert module_name
        return next((m for m in self.modules if m.local_name == module_name), None)

    def resolve_connector(self, global_name: str) -> Connector:
        in_module = self.resolve_module(global_name)
        assert in_module

        connector_name = global_name.split(HierarchicallyNamed.SEPARATOR)[1]
        assert connector_name
        return in_module.resolve_connector(connector_name)

    def resolve_pin(self, global_name: str) -> Pin:
        in_connector = self.resolve_connector(global_name)
        assert in_connector

        pin_name = global_name.split(HierarchicallyNamed.SEPARATOR)[2]
        return in_connector.resolve_pin(pin_name)

    def link_pins(self,
                  pin_ref_a,
                  pin_ref_b):

        if isinstance(pin_ref_a, str):
            pin_a = self.resolve_pin(pin_ref_a)
        elif isinstance(pin_ref_a, Pin):
            pin_a = pin_ref_a
        else:
            raise ValueError("Pin A must either be a global name or pin object")

        if isinstance(pin_ref_b, str):
            pin_b = self.resolve_pin(pin_ref_b)
        elif isinstance(pin_ref_b, Pin):
            pin_b = pin_ref_b
        else:
            raise ValueError("Pin B must either be a global name or pin object")

        assert pin_a
        assert pin_b
        assert pin_a != pin_b

        for net in self._nets:
            if pin_a in net:
                net.add_pin(pin_b)
                return
            elif pin_b in net:
                net.add_pin(pin_a)
                return
        # neither a nor b have been in any nets yet, so create a new one
        new_net = Net()
        new_net.add_pin(pin_a)
        new_net.add_pin(pin_b)
        self._nets.add(new_net)

    def link_connectors(self,
                        connector_ref_a,
                        connector_ref_b):

        if isinstance(connector_ref_a, str):
            connector_a = self.resolve_connector(connector_ref_a)
        elif isinstance(connector_ref_a, Connector):
            connector_a = connector_ref_a
        else:
            raise ValueError("Connector A must either be a global name or connector object")

        if isinstance(connector_ref_b, str):
            connector_b = self.resolve_connector(connector_ref_b)
        elif isinstance(connector_ref_b, Connector):
            connector_b = connector_ref_b
        else:
            raise ValueError("Connector B must either be a global name or connector object")

        assert connector_a
        assert connector_b
        assert connector_a != connector_b
        assert isinstance(connector_b, type(connector_a))  # same type required

        for pin in connector_a.pin_assignment.values():
            pin_name = pin.local_name
            pin_name_a = connector_a.global_name + HierarchicallyNamed.SEPARATOR + pin_name
            pin_name_b = connector_b.global_name + HierarchicallyNamed.SEPARATOR + pin_name
            self.link_pins(pin_name_a, pin_name_b)

    def update_nets(self):

        for net in self._nets:
            net.update()
            for pin in net.dirty:
                scheduled_module = self.resolve_module(pin.global_name)
                self.schedule_module_next_delta(scheduled_module)

    def simulate_delta_cycle(self, current_delta):

        print("\tDelta = " + str(self.current_delta))

        for current_module in self._delta_list[current_delta]:

            print("\t\tUpdating " + current_module.global_name)
            current_module.update()

        print("\n\t\tUpdating nets")
        self.update_nets()

    def simulate_time_step(self):

        print("\n=== === === Next time step === === ===\n")

        self._delta_list.clear()
        self.current_delta = 0

        for net in self._nets:
            net.reset()

        for current_module in self.modules:
            current_module.clear_connectors()
            current_module.clear_state()

        self._delta_list[0] = self.modules

        while self.current_delta in self._delta_list:
            self.simulate_delta_cycle(self.current_delta)
            self.current_delta += 1

        for current_module in self.modules:
            if not current_module.powered or not current_module.grounded:
                print("Module " + current_module.global_name + " not evaluated due to power issues")
