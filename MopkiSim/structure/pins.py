from enum import Enum, auto
from typing import List

from structure.helpers import HierarchicallyNamed


class PinType(Enum):
    POWER = auto()   # +5V
    GROUND = auto()  # +0V
    SIGNAL = auto()  # Logic signal


class SignalLevel(Enum):
    LOW = auto()
    HIGH = auto()
    DISCONNECTED = auto()
    UNDEFINED = auto()


class Pin(HierarchicallyNamed, object):

    def __init__(self,
                 pin_name: str,
                 in_connector: HierarchicallyNamed,
                 pin_type: PinType = PinType.SIGNAL):

        from structure.connectors import Connector

        assert in_connector, "Pin can not be created without being in a connector"
        assert isinstance(in_connector, Connector)

        super(Pin, self).__init__(pin_name, in_connector)

        self.pin_type = pin_type
        self.signal_level = SignalLevel.UNDEFINED

    def drive(self, to_level: SignalLevel):
        self.signal_level = to_level
        # print(self.global_name + " -> " + to_level.name)

    def __str__(self):
        return "Pin " + self.global_name + " (" + self.pin_type.name + ")\n\tState: " + self.signal_level.name


# Pins don't care about directions or such
# it is the modules responsibility to figure out which pn to query and which one to drive...

class SignalPin(Pin):

    def __init__(self,
                 pin_name: str,
                 in_connector: HierarchicallyNamed):

        super(SignalPin, self).__init__(pin_name, in_connector, PinType.SIGNAL)

    def drive(self, to_level: SignalLevel):

        assert to_level == self.signal_level or self.signal_level == SignalLevel.UNDEFINED, \
            "Conflict when driving pin " + self.global_name + \
            ": " + self.signal_level.name + " -> " + to_level.name

        super(SignalPin, self).drive(to_level)

    def reset(self):
        self.signal_level = SignalLevel.UNDEFINED

    def disconnect(self):
        self.signal_level = SignalLevel.DISCONNECTED


class PowerPin(Pin):

    def __init__(self,
                 pin_name: str,
                 in_connector: HierarchicallyNamed):

        super(PowerPin, self).__init__(pin_name, in_connector, PinType.POWER)

    def drive(self, to_level: SignalLevel):
        assert to_level != SignalLevel.LOW, self.global_name + " Attempted to drive a power pin LOW"

        super(PowerPin, self).drive(to_level)


class GroundPin(Pin):

    def __init__(self,
                 pin_name: str,
                 in_connector: HierarchicallyNamed):

        super(GroundPin, self).__init__(pin_name, in_connector, PinType.GROUND)

    def drive(self, to_level: SignalLevel):
        assert to_level != SignalLevel.HIGH, self.global_name + " Attempted to drive a ground pin HIGH"

        super(GroundPin, self).drive(to_level)


def pin_names(pin_type: PinType, index_low: int = 0, index_high: int = 7):

    if pin_type == PinType.SIGNAL:
        pin_prefix = "D"
    elif pin_type == PinType.GROUND:
        pin_prefix = "GND"
    elif pin_type == PinType.POWER:
        pin_prefix = "PWR"
    else:
        pin_prefix = "X"

    names: List[str] = (pin_prefix + str(index) for index in range(index_low, index_high - index_low + 1))
    return names
