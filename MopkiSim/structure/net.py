from typing import Set

from structure.pins import Pin, SignalLevel


class Net(object):
    pass


class Net(object):

    instance_counter: int = 0

    def __init__(self):
        self.pins: Set[Pin] = set()
        self.dirty: Set[Pin] = set()
        self.state: SignalLevel = SignalLevel.UNDEFINED
        self.instance_number = Net.instance_counter
        Net.instance_counter += 1

    def reset(self):
        self.dirty = set()
        self.state = SignalLevel.UNDEFINED

    def add_pin(self, pin: Pin):
        assert pin

        self.pins.add(pin)

    def update(self):

        # Skip empty nets
        if not self.pins:
            return

        # start with an undefined state
        self.state = SignalLevel.UNDEFINED
        self.dirty = {}

        unset_pins: Set[Pin] = \
            set(p for p in self.pins if p.signal_level == SignalLevel.UNDEFINED)

        disconnected_pins: Set[Pin] = \
            set(p for p in self.pins if p.signal_level == SignalLevel.DISCONNECTED)

        driver_pins: Set[Pin] = \
            (self.pins - disconnected_pins) - unset_pins

        if not driver_pins:
            print("\t\t\tNet " + str(self.instance_number) + " has no drivers")
            return

        # assume the first driver as reference

        self.state = next(iter(driver_pins)).signal_level

        print("\t\t\tNet " + str(self.instance_number) + " agreed to state " + self.state.name)

        assert self.state == SignalLevel.HIGH or self.state == SignalLevel.LOW, \
            "Attempting to drive net to an illegal state"

        # check for conflicting driver pins
        for pin in driver_pins:
            if pin.signal_level != self.state:
                print("Net " + str(self.instance_number) + " has drivers that drive to different states")
                print(str(pin) + " disagrees with state " + self.state.name)

        # propagate to all unset pins
        for pin in unset_pins:
            print("\t\t\tDriving Pin " + pin.global_name)
            pin.drive(self.state)

        # mark all recently set pins as dirty
        self.dirty = unset_pins

    def __contains__(self, item):
        return item in self.pins

    def __str__(self):
        return "Net " + str(self.instance_number) + ": state = " + self.state.name
