from unittest import TestCase

from components.modules import PSU, SwitchInput8bit, Adder
from structure.design import Design
from structure.pins import pin_names, PinType, SignalLevel


class AdderTest(TestCase):

    def setUp(self):

        self.chip_enable: bool = False
        self.a_to_b: bool = False

        self.design = Design()

        self.psu: PSU = self.design.add_module(PSU("PSU"))
        self.switch_arg0: SwitchInput8bit = self.design.add_module(SwitchInput8bit("Arg0"))
        self.switch_arg1: SwitchInput8bit = self.design.add_module(SwitchInput8bit("Arg1"))
        self.switch_carry: SwitchInput8bit = self.design.add_module(SwitchInput8bit("CarryIn"))
        self.uut: Adder = self.design.add_module(Adder("UUT"))

        assert self.psu
        assert self.switch_arg0
        assert self.switch_arg1
        assert self.switch_carry
        assert self.uut

        self.design.link_connectors(self.switch_arg0.conn_output, self.uut.conn_arg0)
        self.design.link_connectors(self.switch_arg1.conn_output, self.uut.conn_arg1)

        self.design.link_pins(self.psu.conn_power_out.pin_pwr, self.uut.conn_result.pin_pwr)
        self.design.link_pins(self.psu.conn_power_out.pin_gnd, self.uut.conn_result.pin_gnd)
        self.design.link_pins(self.psu.conn_power_out.pin_pwr, self.switch_carry.conn_output.pin_pwr)
        self.design.link_pins(self.psu.conn_power_out.pin_gnd, self.switch_carry.conn_output.pin_gnd)

        self.design.link_pins(self.switch_carry.conn_output.pin_d0, self.uut.conn_carry_in.pin_d0)

    def test_zero(self):

        for pin in pin_names(PinType.SIGNAL, 0, 7):
            self.switch_arg0.disable(pin)
            self.switch_arg1.disable(pin)
            self.switch_carry.disable(pin)

        self.design.simulate_time_step()

        result_state = self.uut.conn_result.get_state()
        for pin in pin_names(PinType.SIGNAL, 0, 7):
            self.assertEqual(result_state[pin], SignalLevel.LOW)

        self.assertEqual(self.uut.conn_zero.get_state()["D0"], SignalLevel.HIGH)

    def test_sign(self):

        self.switch_arg0.enable("D7")

        self.design.simulate_time_step()

        self.assertEqual(self.uut.conn_sign.get_state()["D0"], SignalLevel.HIGH)
