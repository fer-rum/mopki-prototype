import random
from typing import Dict
from unittest import TestCase

from components.modules import Transceiver, PSU, SwitchInput8bit
from structure.design import Design
from structure.pins import SignalLevel, pin_names, PinType


def generate_random_state():

    new_state: Dict[str, SignalLevel] = {}

    for name in pin_names(PinType.SIGNAL, 0, 7):
        new_state[name] = random.choice(seq=[SignalLevel.LOW, SignalLevel.HIGH])

    return new_state


class TransceiverTest(TestCase):

    def setUp(self):

        #  ctrl.d0 -> uut.ce
        #  ctrl.d1 -> uut.a_to_b

        self.design = Design()

        self.psu: PSU = self.design.add_module(PSU("PSU"))
        self.data_in: SwitchInput8bit = self.design.add_module(SwitchInput8bit("DataIn"))
        self.ctrl_in: SwitchInput8bit = self.design.add_module(SwitchInput8bit("CtrlIn"))
        self.uut: Transceiver = self.design.add_module(Transceiver("UUT"))

        assert self.psu
        assert self.data_in
        assert self.ctrl_in
        assert self.uut

        self.design.link_connectors(self.data_in.conn_output, self.uut.conn_side_a)

        self.design.link_pins(self.psu.conn_power_out.pin_pwr, self.data_in.conn_output.pin_pwr)
        self.design.link_pins(self.psu.conn_power_out.pin_gnd, self.data_in.conn_output.pin_gnd)
        self.design.link_pins(self.psu.conn_power_out.pin_pwr, self.ctrl_in.conn_output.pin_pwr)
        self.design.link_pins(self.psu.conn_power_out.pin_gnd, self.ctrl_in.conn_output.pin_gnd)

        self.design.link_pins(self.ctrl_in.conn_output.pin_d0, self.uut.conn_chip_enable.pin_d0)
        self.design.link_pins(self.ctrl_in.conn_output.pin_d1, self.uut.conn_direction.pin_d0)

    def test_pass_through(self):

        in_state = generate_random_state()
        self.data_in.set_state(in_state)

        # set CE, AtoB
        self.ctrl_in.enable("D0")
        self.ctrl_in.enable("D1")

        self.design.simulate_time_step()

        a_state = self.uut.conn_side_a.get_state()
        b_state = self.uut.conn_side_b.get_state()

        for key in in_state:
            self.assertEqual(in_state[key], a_state[key])
            self.assertEqual(in_state[key], b_state[key])

    def test_no_pass(self):

        in_state = generate_random_state()
        self.data_in.set_state(in_state)

        # set AtoB
        # leave CE low
        self.ctrl_in.disable("D0")
        self.ctrl_in.enable("D1")

        self.design.simulate_time_step()

        a_state = self.uut.conn_side_a.get_state()
        b_state = self.uut.conn_side_b.get_state()

        for key in in_state:
            self.assertEqual(a_state[key], SignalLevel.DISCONNECTED)
            self.assertEqual(b_state[key], SignalLevel.DISCONNECTED)


